package com.app.common;

import java.awt.AWTException;
import java.awt.Image;
import java.awt.SystemTray;
import java.awt.Toolkit;
import java.awt.TrayIcon;
import java.awt.TrayIcon.MessageType;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import javax.swing.JOptionPane;

import com.app.InfoSystem;
import com.app.config.Config;
import com.app.controller.changer.Changer;
import com.app.controller.manager.Navigation;
import com.app.controller.whether.CWhether;
import com.app.model.Alarm;
import com.app.model.Huyen;
import com.app.model.WhetherInfo;
import com.app.model.Xa;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javazoom.jl.decoder.JavaLayerException;
import javazoom.jl.player.Player;

public class Tool {
	public static void loadInfoSystem() {
		getInfoSystem();
		getRecentBG();
		loadHuyen();
	}

	public static Player player = null;
	public static Player playerAlarm = null;
	public static boolean flag = true;

	private static void getRecentBG() {
		BufferedReader reader = null;
		try {
			reader = new BufferedReader(new FileReader("listReBackground.list"));
			String path = reader.readLine();
			while (path != null) {
				InfoSystem.listRecentBG.add(path);
				path = reader.readLine();
			}
			reader.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private static void getInfoSystem() {
		String path = "config\\configsys.ini";
		BufferedReader reader = null;
		String re = "";
		try {
			reader = new BufferedReader(new FileReader(path));
			String t = reader.readLine();
			while (t != null) {
				re += t;
				t = reader.readLine();
			}
			reader.close();
			JsonObject data = new Gson().fromJson(re, JsonObject.class);
			InfoSystem.autoChangeBG = data.get("autoChangeBG").getAsBoolean();
			InfoSystem.autoUpdateWhether = data.get("autoUpdateWhether").getAsBoolean();
			InfoSystem.chooseFolderBG = data.get("pathFolderBG").getAsString();
			InfoSystem.isShutUp = data.get("shutup").getAsBoolean();
			InfoSystem.huyen = data.get("huyen").getAsInt();
			InfoSystem.xa = data.get("xa").getAsInt();
			if (InfoSystem.chooseFolderBG != null) {
				if (InfoSystem.chooseFolderBG.equals("")) {
					InfoSystem.chooseFolderBG = null;
				}
			}
			InfoSystem.chooseFolderMS = data.get("pathFolderMS").getAsString();
			if (InfoSystem.chooseFolderMS != null) {
				if (InfoSystem.chooseFolderMS.equals("")) {
					InfoSystem.chooseFolderMS = null;
				}
			}
			InfoSystem.playMusic = data.get("playMusic").getAsBoolean();
			InfoSystem.titleApp = data.get("title").getAsString();
			if (InfoSystem.titleApp == null)
				InfoSystem.titleApp = Config.titleApp;
			if (InfoSystem.titleApp.equals(""))
				InfoSystem.titleApp = Config.titleApp;
			InfoSystem.topic = data.get("topic").getAsString();
			if (InfoSystem.topic == null)
				InfoSystem.topic = Config.topic;
			if (InfoSystem.topic.equals(""))
				InfoSystem.topic = Config.topic;

			if (!data.get("sohot").getAsString().equals("#")) {
				InfoSystem.pathSun = data.get("sohot").getAsString();
			}
			if (!data.get("sun").getAsString().equals("#")) {
				InfoSystem.pathHaveSun = data.get("sun").getAsString();
			}
			if (!data.get("cloudy").getAsString().equals("#")) {
				InfoSystem.pathSunCloud = data.get("cloudy").getAsString();
			}
			if (!data.get("socloudy").getAsString().equals("#")) {
				InfoSystem.pathMuchCloud = data.get("socloudy").getAsString();
			}
			if (!data.get("giong").getAsString().equals("#")) {
				InfoSystem.pathGiongBao = data.get("giong").getAsString();
			}
			if (!data.get("rain").getAsString().equals("#")) {
				InfoSystem.pathRain = data.get("rain").getAsString();
			}
			if (!data.get("cool").getAsString().equals("#")) {
				InfoSystem.pathCool = data.get("cool").getAsString();
			}
			if (!data.get("cold").getAsString().equals("#")) {
				InfoSystem.pathCold = data.get("cold").getAsString();
			}
			if (!data.get("other").getAsString().equals("#")) {
				InfoSystem.pathOther = data.get("other").getAsString();
			}
		} catch (Exception e) {
			System.out.println(re);
			e.printStackTrace();
		}
	}

	private static void loadHuyen() {
		String jsonStr = "";
		BufferedReader reader = null;
		try {
			reader = new BufferedReader(new FileReader("huyenxa.json"));
			jsonStr = reader.readLine();
			reader.close();
			Huyen[] hs = new Gson().fromJson(jsonStr, Huyen[].class);
			for (Huyen h : hs) {
				InfoSystem.hs.add(h);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void writeRecentBG() {
		BufferedWriter writer = null;
		try {
			writer = new BufferedWriter(new FileWriter("listReBackground.list"));
			for (String path : InfoSystem.listRecentBG) {
				writer.write(path);
				writer.newLine();
			}
			writer.flush();
			writer.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void writeInfoSystem() {
		BufferedWriter writer = null;
		try {
			writer = new BufferedWriter(new FileWriter("config\\configsys.ini"));
			JsonObject data = new JsonObject();
			data.addProperty("autoChangeBG", InfoSystem.autoChangeBG);
			data.addProperty("autoUpdateWhether", InfoSystem.autoUpdateWhether);
			data.addProperty("pathFolderBG", (InfoSystem.chooseFolderBG == null) ? "" : InfoSystem.chooseFolderBG);
			data.addProperty("pathFolderMS", (InfoSystem.chooseFolderMS == null) ? "" : InfoSystem.chooseFolderMS);
			data.addProperty("playMusic", InfoSystem.playMusic);
			data.addProperty("title", InfoSystem.titleApp);
			data.addProperty("topic", InfoSystem.topic);
			data.addProperty("shutup", InfoSystem.isShutUp);

			data.addProperty("sohot", InfoSystem.pathSun);
			data.addProperty("sun", InfoSystem.pathHaveSun);
			data.addProperty("cloudy", InfoSystem.pathSunCloud);
			data.addProperty("socloudy", InfoSystem.pathMuchCloud);
			data.addProperty("giong", InfoSystem.pathGiongBao);
			data.addProperty("rain", InfoSystem.pathRain);
			data.addProperty("cool", InfoSystem.pathCool);
			data.addProperty("cold", InfoSystem.pathCold);
			data.addProperty("other", InfoSystem.pathOther);

			data.addProperty("huyen", InfoSystem.huyen);
			data.addProperty("xa", InfoSystem.xa);
			writer.write(new Gson().toJson(data));
			writer.flush();
			writer.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void runConfig() {
		if (InfoSystem.autoUpdateWhether == true) {
			if (InfoSystem.isRunThreadUpdate == false) {
				ScheduledFuture<?> s = InfoSystem.scheduler.scheduleWithFixedDelay(new Runnable() {

					public void run() {
						synchronized (InfoSystem.we) {
							InfoSystem.we = new CWhether().getWhether();
							Tool.writeWhether();
							if (InfoSystem.currentTab == Config.tabWhether) {
								new Navigation().goToMainPage(InfoSystem.viewPanel);
							}
							if (InfoSystem.autoChangeBG == true) {
								setBGWithWhether();
							}
						}
					}
				}, 0, Config.timeUpdate, TimeUnit.MINUTES);
				InfoSystem.taskIds.put("updateWhether", s);
				InfoSystem.isRunThreadUpdate = true;
			}
		} else {
			if (InfoSystem.isRunThreadUpdate == true) {
				ScheduledFuture<?> s = InfoSystem.taskIds.get("updateWhether");
				s.cancel(false);
				InfoSystem.taskIds.remove("updateWhether");
				InfoSystem.isRunThreadUpdate = false;
			}
		}

		playMusicOrigin();
	}
	
	private static void playMusicOrigin() {
		if (InfoSystem.playMusic == true) {
			if (InfoSystem.isPlayMusic == false) {
				if (InfoSystem.chooseFolderMS == null) {
					return;
				}
				if (InfoSystem.chooseFolderMS.equals("")) {
					return;
				}
				flag = true;
				ScheduledFuture<?> s = InfoSystem.scheduler.scheduleWithFixedDelay(new Runnable() {

					public void run() {
						File dir = new File(InfoSystem.chooseFolderMS);
						if (dir.isDirectory() == true) {
							File[] files = dir.listFiles();
							if (files.length == 0) {
								InfoSystem.lblAlarm.setText("Trong thư mục không có bài hát nào");
							}
							for (File file : files) {
								if (flag == false)
									break;
								if (file.getName().endsWith(".mp3") | file.getName().endsWith(".wav")) {
									FileInputStream input;
									try {
										input = new FileInputStream(file.getAbsoluteFile());
										player = new Player(input);
										player.play();
									} catch (FileNotFoundException e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									} catch (JavaLayerException e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									}

								}
							}
						}
					}
				}, 0, 1, TimeUnit.SECONDS);
				InfoSystem.taskIds.put("playMusic", s);
				InfoSystem.isPlayMusic = true;
			}
		} else {
			if (InfoSystem.isPlayMusic == true) {
				ScheduledFuture<?> s = InfoSystem.taskIds.get("playMusic");
				s.cancel(false);
				player.close();
				player = null;
				flag = false;
				InfoSystem.taskIds.remove("playMusic");
				InfoSystem.isPlayMusic = false;
			}
		}
	}

	public static void setBGWithWhether() {
		if (InfoSystem.we == null) {
			return;
		}
		WhetherInfo w = InfoSystem.we.getWhetherInfo();
		try {
			int temp = Integer.parseInt(w.getTm());
			if (w.getDc().contains("nắng")) {
				if (temp > 37) {
					new Changer().changeBackgroundDesktop(new File(InfoSystem.pathSun).getAbsolutePath());
					return;
				}
				new Changer().changeBackgroundDesktop(new File(InfoSystem.pathHaveSun).getAbsolutePath());
				return;
			}
			if (w.getDc().contains("Có mây")) {
				new Changer().changeBackgroundDesktop(new File(InfoSystem.pathSunCloud).getAbsolutePath());
				return;
			}
			if (w.getDc().contains("Nhiều mây")) {
				new Changer().changeBackgroundDesktop(new File(InfoSystem.pathMuchCloud).getAbsolutePath());
				return;
			}
			if (w.getDc().contains("giông")) {
				new Changer().changeBackgroundDesktop(new File(InfoSystem.pathGiongBao).getAbsolutePath());
				return;
			}
			if (w.getDc().contains("mưa")) {
				new Changer().changeBackgroundDesktop(new File(InfoSystem.pathRain).getAbsolutePath());
				return;
			}
			if (temp > 20 & temp <= 30) {
				new Changer().changeBackgroundDesktop(new File(InfoSystem.pathCool).getAbsolutePath());
				return;
			}
			if (temp <= 20) {
				new Changer().changeBackgroundDesktop(new File(InfoSystem.pathCold).getAbsolutePath());
				return;
			}
			new Changer().changeBackgroundDesktop(new File(InfoSystem.pathOther).getAbsolutePath());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void writeWhether() {
		if (InfoSystem.we != null) {
			try {
				Calendar ca = Calendar.getInstance();
				BufferedWriter writer = new BufferedWriter(new FileWriter("whetherToday"));
				writer.write(ca.getTimeInMillis() + "");
				writer.newLine();
				writer.write(new Gson().toJson(InfoSystem.we));
				writer.flush();
				writer.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	public static void writeAlarm(List<Alarm> as) {
		BufferedWriter writer = null;
		Gson g = new Gson();
		try {
			writer = new BufferedWriter(new FileWriter("data.dxt"));
			if (as.size() > 0) {
				for (Alarm a : as) {
					writer.write(g.toJson(a));
					writer.newLine();
				}
			} else {
				writer.write("");
			}
			writer.flush();
			writer.close();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	public static void turnOnAlarm(Alarm a) {
		if (checkCreateTaskAlarm(a) == false) {
			return;
		}
		System.out.println("over test");
		Calendar ca = Calendar.getInstance();
		Calendar cc = Calendar.getInstance();
		cc.setTimeInMillis(a.getTime());
		ca.set(Calendar.HOUR_OF_DAY, cc.get(Calendar.HOUR_OF_DAY));
		ca.set(Calendar.MINUTE, cc.get(Calendar.MINUTE));
		ca.set(Calendar.SECOND, 0);
		long timeDelay = (ca.getTimeInMillis() - new Date().getTime()) / 1000;
		System.out.println("time delay: " + timeDelay + " s");
		ScheduledFuture<?> s = InfoSystem.scheduler.schedule(new Runnable() {

			@Override
			public void run() {
				try {
					Alarm b = a;
					if(player != null) {
						ScheduledFuture<?> s = InfoSystem.taskIds.get("playMusic");
						s.cancel(false);
						player.close();
						player = null;
						flag = false;
						InfoSystem.taskIds.remove("playMusic");
						InfoSystem.isPlayMusic = false;
					}
					if (a.getTypeEnd() == 2) {
						a.setTimeExe(a.getTimeExe() + 1);
					}
					System.out.println("kaka");
					displayNoticeForWindow(b.getMes());
					JOptionPane.showConfirmDialog(null, b.getMes());
					if(player != null) {
						player.close();
						playMusicOrigin();
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}, timeDelay, TimeUnit.SECONDS);
		ScheduledFuture<?> s2 = InfoSystem.scheduler.schedule(new Runnable() {

			@Override
			public void run() {
				try {
					File file = new File("alarm.mp3");
					FileInputStream input = new FileInputStream(file);
					player = new Player(input);
					player.play();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}, timeDelay+1, TimeUnit.SECONDS);
		InfoSystem.taskIds.put(a.getUid().toString(), s);
		InfoSystem.taskIds.put(a.getUid()+"music".toString(), s2);
	}

	private static boolean checkCreateTaskAlarm(Alarm a) {
		if (a.getStatus() == 1) {
			System.out.println("huy do alarm bi tat");
			return false;
		}
		if (a.getType() == 0) {
			if (a.getTime() < new Date().getTime()) {
				System.out.println("Huy do thoi gian goi da qua");
				return false;
			}
		} else {
			Calendar ca = Calendar.getInstance();
			Calendar cc = Calendar.getInstance();
			cc.setTimeInMillis(a.getTime());
			ca.set(Calendar.HOUR_OF_DAY, cc.get(Calendar.HOUR_OF_DAY));
			ca.set(Calendar.MINUTE, cc.get(Calendar.MINUTE));
			if (ca.getTimeInMillis() < new Date().getTime()) {
				System.out.println("huy do dat che do tuy chinh vs thoi gian goi da qua");
				return false;
			}
			if (a.getType() == 5) {
				if (a.getTypeEnd() == 2) {
					if (a.getTimeExe() >= a.getTimeLimitExe()) {
						System.out.println("Huy do so lan lap da vuot gioi han");
						return false;
					}
				} else if (a.getTypeEnd() == 1) {
					if (ca.getTimeInMillis() > a.getTimeFinish()) {
						System.out.println("huy do da qua ngay han lap");
						return false;
					}
				}
				// 2 3 4 5 6 7 8
				// 2 3 4 5 6 7 1
				try {
					if (a.getTypeLoop() == 1) {
						int[] dates = a.getDateLoop();
						boolean flag = false;
						for (int i : dates) {
							int ss = (i + 2 > 7) ? 1 : i + 2;
							if (ss == ca.get(Calendar.DAY_OF_WEEK)) {
								flag = true;
								break;
							}
						}
						if (flag == false) {
							System.out.println("Huy do ko phai ngay da dat");
							return false;
						}
					} else if (a.getTypeLoop() == 2) {
						if (a.getTypeLoopByMonth() == 0) {
							if (ca.get(Calendar.DAY_OF_MONTH) != cc.get(Calendar.DAY_OF_MONTH))
								return false;
						} else if (a.getTypeLoopByMonth() == 1) {
							if (ca.get(Calendar.DAY_OF_WEEK) != cc.get(Calendar.DAY_OF_WEEK)
									| ca.get(Calendar.WEEK_OF_MONTH) != cc.get(Calendar.WEEK_OF_MONTH))
								return false;
						} else {
							if (ca.get(Calendar.DAY_OF_WEEK) != cc.get(Calendar.DAY_OF_WEEK)
									| ca.get(Calendar.DAY_OF_WEEK_IN_MONTH) != cc.get(Calendar.DAY_OF_WEEK_IN_MONTH))
								return false;
						}
					} else if (a.getTypeLoop() == 3) {
						if (ca.get(Calendar.DAY_OF_MONTH) != cc.get(Calendar.DAY_OF_MONTH)
								| ca.get(Calendar.MONTH) != cc.get(Calendar.MONTH)) {
							return false;
						}
					}
				} catch (Exception e) {
					System.out.println("Huy do co loi");
					return false;
				}
			} else if (a.getType() == 4) {// hang nam
				if (ca.get(Calendar.DAY_OF_YEAR) != cc.get(Calendar.DAY_OF_YEAR))
					return false;
			} else if (a.getType() == 3) {// hang thang
				if (ca.get(Calendar.DAY_OF_MONTH) != cc.get(Calendar.DAY_OF_MONTH))
					return false;
			} else if (a.getType() == 2) {// hang tuan
				if (ca.get(Calendar.DAY_OF_WEEK) != cc.get(Calendar.DAY_OF_WEEK))
					return false;
			}
		}
		return true;
	}
	
	private static void displayNoticeForWindow(String mes) {
		 SystemTray tray = SystemTray.getSystemTray();

	        //If the icon is a file
	        Image image = Toolkit.getDefaultToolkit().createImage("icon.png");
	        //Alternative (if the icon is on the classpath):
	        //Image image = Toolkit.getDefaultToolkit().createImage(getClass().getResource("icon.png"));

	        TrayIcon trayIcon = new TrayIcon(image, "Tray Demo");
	        //Let the system resize the image if needed
	        trayIcon.setImageAutoSize(true);
	        //Set tooltip text for the tray icon
	        trayIcon.setToolTip("System tray icon demo");
	        try {
				tray.add(trayIcon);
			} catch (AWTException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

	        trayIcon.displayMessage("Dxtruong notify", mes, MessageType.INFO);
	}
}
