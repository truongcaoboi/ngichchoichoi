package com.app.impl;

import java.util.HashMap;

import com.sun.jna.Native;
import com.sun.jna.win32.*;

public interface IChanger extends StdCallLibrary{
	@SuppressWarnings("serial")
	IChanger changer = (IChanger) Native.loadLibrary("user32", IChanger.class, new HashMap<String , Object>(){
		{
			put(OPTION_TYPE_MAPPER, W32APITypeMapper.UNICODE);
			put(OPTION_FUNCTION_MAPPER, W32APIFunctionMapper.UNICODE);
		}
	});
	
	boolean SystemParametersInfo(int a, int b, String path, int c);
}
