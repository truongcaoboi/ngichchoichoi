package com.app.impl;

import java.util.List;

import javax.swing.JTable;

import com.app.model.Alarm;

public interface IAlarm {
	public List<Alarm> getAlarms();
	public Alarm getAlarmsById(int id, List<Alarm> as);
	public void saveAlarm(Alarm a, JTable table);
	public void addAlarm(Alarm a, JTable table);
	public void deleteAlarm(Alarm a, JTable table);
	public Object[][] getInfoAlarms(List<Alarm> as);
	public Object [] getInfoAlarm(Alarm a);
	public void turnOnAlarm(Alarm a, JTable table);
	public void turnOffAlarm(Alarm a, JTable table);
}
