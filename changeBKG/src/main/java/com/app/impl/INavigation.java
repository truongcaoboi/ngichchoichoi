package com.app.impl;

import javax.swing.JPanel;

public interface INavigation {
	public void goToMainPage(JPanel parent) ;
	
	public void goToSettingPage(JPanel parent);
	
	public void goToManagerPage(JPanel parent);
	
	public void goToWhetherPage(JPanel parent);
	
	public void goToAlarmPage(JPanel parent);
	
	public void goToChageBGDesktop(JPanel parent);
}
