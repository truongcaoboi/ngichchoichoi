package com.app.view;

import javax.swing.JPanel;

import com.app.InfoSystem;
import com.app.common.Tool;
import com.app.config.Config;
import com.app.controller.manager.Navigation;
import com.app.controller.whether.CWhether;
import com.app.model.Huyen;
import com.app.model.Xa;

import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;
import java.awt.event.ActionEvent;
import java.awt.Color;
import javax.swing.JTextField;
import javax.swing.JCheckBox;
import javax.swing.JFileChooser;
import javax.swing.JTextPane;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import java.awt.event.InputMethodListener;
import java.awt.event.InputMethodEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeEvent;
import java.awt.event.ItemListener;
import java.awt.event.ItemEvent;

public class VSetting extends JPanel {

	/**
	 * Create the panel.
	 */
	private JPanel myParent;
	private JTextField txtTitle;
	private JTextField txttopic;
	private JLabel lblNameFolderBG, lblNameFolderMS;
	private JCheckBox cbAutoChangeBG, cbAutoUpdateWhether, cbPlayMusic;
	private JComboBox cbxHuyen, cbxXa;

	public VSetting(JPanel parent) {
		myParent = parent;
		this.setBounds(0, 0, Config.widthPageMain, Config.heightPageMain);
		setLayout(null);

		JLabel lblNewLabel = new JLabel("Thiết lập hệ thống");
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblNewLabel.setBounds(10, 16, 212, 25);
		add(lblNewLabel);

		JButton btnNewButton = new JButton("Quay lại");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new Navigation().goToMainPage(myParent);
			}
		});
		btnNewButton.setBounds(1115, 8, 89, 33);
		add(btnNewButton);

		JPanel panel = new JPanel();
		panel.setBackground(Color.WHITE);
		panel.setBounds(20, 52, 566, 602);
		add(panel);
		panel.setLayout(null);

		JLabel lblNewLabel_1 = new JLabel("Tiêu đề");
		lblNewLabel_1.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblNewLabel_1.setBounds(10, 11, 60, 22);
		panel.add(lblNewLabel_1);

		txtTitle = new JTextField();
		txtTitle.setFont(new Font("Tahoma", Font.PLAIN, 12));
		txtTitle.setBounds(80, 9, 476, 27);
		txtTitle.setText(InfoSystem.titleApp);
		panel.add(txtTitle);
		txtTitle.setColumns(10);

		JLabel label = new JLabel("Chủ đề");
		label.setFont(new Font("Tahoma", Font.PLAIN, 12));
		label.setBounds(10, 50, 60, 22);
		panel.add(label);

		txttopic = new JTextField();
		txttopic.setFont(new Font("Tahoma", Font.PLAIN, 12));
		txttopic.setColumns(10);
		txttopic.setText(InfoSystem.topic);
		txttopic.setBounds(80, 48, 476, 27);
		panel.add(txttopic);

		lblNameFolderBG = new JLabel((InfoSystem.chooseFolderBG == null) ? "Chọn thư mục chứa ảnh background"
				: (InfoSystem.chooseFolderBG.equals(""))
						? "'dmm' is message which is sent by dxtruong :)) ==> Folder MS"
						: InfoSystem.chooseFolderBG);
		lblNameFolderBG.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblNameFolderBG.setForeground(Color.BLUE);
		lblNameFolderBG.setBounds(161, 86, 395, 33);
		panel.add(lblNameFolderBG);

		JButton btnNewButton_1 = new JButton("Chọn folder BG");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JFileChooser chooser = new JFileChooser();
				chooser.setCurrentDirectory(new File(System.getProperty("user.home")));
				chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
				int result = chooser.showOpenDialog(myParent);
				if (result == JFileChooser.APPROVE_OPTION) {
					lblNameFolderBG.setText(chooser.getSelectedFile().getAbsolutePath());
				}
			}
		});
		btnNewButton_1.setFont(new Font("Tahoma", Font.PLAIN, 12));
		btnNewButton_1.setBounds(10, 86, 141, 33);
		panel.add(btnNewButton_1);

		cbAutoUpdateWhether = new JCheckBox("Tự động cập nhật thời tiết");
		cbAutoUpdateWhether.setSelected(InfoSystem.autoUpdateWhether);
		cbAutoUpdateWhether.setFont(new Font("Tahoma", Font.PLAIN, 12));
		cbAutoUpdateWhether.setBounds(10, 136, 546, 33);
		panel.add(cbAutoUpdateWhether);

		cbAutoChangeBG = new JCheckBox("Tự động thay đổi background desktop theo thời tiết");
		cbAutoChangeBG.setFont(new Font("Tahoma", Font.PLAIN, 12));
		cbAutoChangeBG.setSelected(InfoSystem.autoChangeBG);
		cbAutoChangeBG.setBounds(10, 210, 546, 33);
		panel.add(cbAutoChangeBG);

		cbPlayMusic = new JCheckBox("Phát nhạc trong thư mục lựa chọn");
		cbPlayMusic.setFont(new Font("Tahoma", Font.PLAIN, 12));
		cbPlayMusic.setBounds(10, 255, 546, 33);
		cbPlayMusic.setSelected(InfoSystem.playMusic);
		panel.add(cbPlayMusic);

		JPanel panel_2 = new JPanel();
		panel_2.setBackground(Color.ORANGE);
		panel_2.setBounds(10, 544, 546, 3);
		panel.add(panel_2);

		JButton btnDefault = new JButton("Lấy mặc định");
		btnDefault.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

			}
		});
		btnDefault.setFont(new Font("Tahoma", Font.PLAIN, 12));
		btnDefault.setBounds(206, 556, 110, 33);
		panel.add(btnDefault);

		JButton btnCancel = new JButton("Hủy");
		btnCancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnCancel.setFont(new Font("Tahoma", Font.PLAIN, 12));
		btnCancel.setBounds(446, 556, 110, 33);
		panel.add(btnCancel);

		JButton btnUpdate = new JButton("Thay đổi");
		btnUpdate.setFont(new Font("Tahoma", Font.PLAIN, 12));
		btnUpdate.setBounds(326, 556, 110, 33);
		panel.add(btnUpdate);

		JLabel label_1 = new JLabel("Quận/Huyện");
		label_1.setFont(new Font("Tahoma", Font.PLAIN, 12));
		label_1.setBounds(10, 178, 76, 22);
		panel.add(label_1);

		JButton button = new JButton("Chọn folder MS");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JFileChooser chooser = new JFileChooser();
				chooser.setCurrentDirectory(new File(System.getProperty("user.home")));
				chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
				int result = chooser.showOpenDialog(myParent);
				if (result == JFileChooser.APPROVE_OPTION) {
					lblNameFolderMS.setText(chooser.getSelectedFile().getAbsolutePath());
				}
			}
		});
		button.setFont(new Font("Tahoma", Font.PLAIN, 12));
		button.setBounds(10, 301, 141, 33);
		panel.add(button);

		lblNameFolderMS = new JLabel((InfoSystem.chooseFolderMS == null) ? "Chọn thư mục chứa nhạc"
				: (InfoSystem.chooseFolderMS.equals(""))
						? "'dmm' is message which is sent by dxtruong :)) ==> Folder MS"
						: InfoSystem.chooseFolderMS);
		lblNameFolderMS.setForeground(Color.BLUE);
		lblNameFolderMS.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblNameFolderMS.setBounds(161, 301, 395, 33);
		panel.add(lblNameFolderMS);

		JLabel label_2 = new JLabel("Xã/Phường");
		label_2.setFont(new Font("Tahoma", Font.PLAIN, 12));
		label_2.setBounds(290, 176, 76, 22);
		panel.add(label_2);

		List<String> huyen = new ArrayList<String>();
		List<String> xa = new ArrayList<String>();

		for (Huyen h : InfoSystem.hs) {
			huyen.add(h.getName());
			if (h.getCode() == InfoSystem.huyen) {
				for (Xa x : h.getXas()) {
					xa.add(x.getName());
				}
			}
		}
		cbxHuyen = new JComboBox(huyen.toArray());
		cbxHuyen.setFont(new Font("Tahoma", Font.PLAIN, 12));
		cbxHuyen.setBounds(96, 176, 184, 27);
		if (huyen.size() > 0) {
			cbxHuyen.setSelectedIndex(InfoSystem.huyen);
		}
		panel.add(cbxHuyen);

		cbxXa = new JComboBox(xa.toArray());
		cbxXa.setFont(new Font("Tahoma", Font.PLAIN, 12));
		cbxXa.setBounds(372, 176, 184, 27);
		if (xa.size() > 0) {
			cbxXa.setSelectedIndex(InfoSystem.xa);
		}
		panel.add(cbxXa);

		JPanel panel_1 = new JPanel();
		panel_1.setBackground(Color.WHITE);
		panel_1.setBounds(596, 50, 608, 604);
		add(panel_1);
		panel_1.setLayout(null);

		///
		btnDefault.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				txtTitle.setText(Config.titleApp);
				txttopic.setText(Config.topic);
				lblNameFolderBG.setText("Chọn thư mục chứa ảnh background");
				lblNameFolderMS.setText("Chọn thư mục chứa nhạc");
				cbAutoChangeBG.setSelected(false);
				cbAutoUpdateWhether.setSelected(false);
				cbPlayMusic.setSelected(false);
				cbxHuyen.setSelectedIndex(0);
				cbxXa.setSelectedIndex(0);
			}
		});
		btnCancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				txtTitle.setText(InfoSystem.titleApp);
				txttopic.setText(InfoSystem.topic);
				lblNameFolderBG.setText((InfoSystem.chooseFolderBG == null) ? "Chọn thư mục chứa ảnh background"
						: (InfoSystem.chooseFolderBG.equals(""))
								? "'dmm' is message which is sent by dxtruong :)) ==> Folder MS"
								: InfoSystem.chooseFolderBG);
				lblNameFolderMS.setText((InfoSystem.chooseFolderMS == null) ? "Chọn thư mục chứa nhạc"
						: (InfoSystem.chooseFolderMS.equals(""))
								? "'dmm' is message which is sent by dxtruong :)) ==> Folder MS"
								: InfoSystem.chooseFolderMS);
				cbAutoChangeBG.setSelected(InfoSystem.autoChangeBG);
				cbAutoUpdateWhether.setSelected(InfoSystem.autoUpdateWhether);
				cbPlayMusic.setSelected(InfoSystem.playMusic);
				cbxHuyen.setSelectedIndex(InfoSystem.huyen);
				cbxXa.setSelectedIndex(InfoSystem.xa);
			}
		});
		btnUpdate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				InfoSystem.titleApp = txtTitle.getText();
				InfoSystem.mainFrame.setTitle(InfoSystem.titleApp);
				InfoSystem.topic = txttopic.getText();
				InfoSystem.lblTopic.setText(InfoSystem.topic);
				InfoSystem.autoChangeBG = cbAutoChangeBG.isSelected();
				InfoSystem.autoUpdateWhether = cbAutoUpdateWhether.isSelected();
				InfoSystem.playMusic = cbPlayMusic.isSelected();
				if(InfoSystem.huyen != cbxHuyen.getSelectedIndex() || InfoSystem.xa != cbxXa.getSelectedIndex()) {
					InfoSystem.huyen = cbxHuyen.getSelectedIndex();
					InfoSystem.xa = cbxXa.getSelectedIndex();
					if (InfoSystem.huyen < 0) {
						InfoSystem.huyen = 0;
					}
					if (InfoSystem.xa < 0) {
						InfoSystem.xa = 0;
					}
					InfoSystem.we = new CWhether().getFromWeb();
					Tool.writeWhether();
				}
				if (!lblNameFolderBG.getText().equals("Chọn thư mục chứa ảnh background") & !lblNameFolderBG.getText()
						.equals("'dmm' is message which is sent by dxtruong :)) ==> Folder M")) {
					InfoSystem.chooseFolderBG = lblNameFolderBG.getText();
				}
				if (!lblNameFolderMS.getText().equals("Chọn thư mục chứa nhạc") & !lblNameFolderMS.getText()
						.equals("'dmm' is message which is sent by dxtruong :)) ==> Folder MS")) {
					InfoSystem.chooseFolderMS = lblNameFolderMS.getText();
				}
				Tool.runConfig();
				JOptionPane.showMessageDialog(null, "Thay đổi thành công");
			}
		});
		cbxHuyen.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				int index = cbxHuyen.getSelectedIndex();
				Huyen hc = null;
				for (Huyen h : InfoSystem.hs) {
					if (h.getCode() == index) {
						hc = h;
					}
				}
				List<String> x = new ArrayList<String>();
				for (Xa a : hc.getXas()) {
					x.add(a.getName());
				}
				cbxXa.setModel(new DefaultComboBoxModel(x.toArray()));
				if (x.size() > 0) {
					cbxXa.setSelectedIndex(0);
				}
//				cbxXa.revalidate();
//				cbxXa.repaint();
			}
		});
	}

}
