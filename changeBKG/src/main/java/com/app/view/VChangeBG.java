package com.app.view;

import javax.swing.JPanel;

import com.app.InfoSystem;
import com.app.config.Config;
import com.app.controller.changer.Changer;

import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import java.awt.Color;
import java.awt.Dimension;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFileChooser;

import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.awt.event.ActionEvent;
import javax.swing.SwingConstants;
import javax.swing.filechooser.FileFilter;
import javax.swing.JScrollPane;

public class VChangeBG extends JPanel {

	/**
	 * Create the panel.
	 */
	private JPanel myParent, panelReImage, panel;
	private File chooseFile = null;
	private JLabel lblNameFile;
	private JButton btnImageOne, btnImageTwo, btnImageThree, btnImageFour, btnImageFive, btnImageSix;

	public VChangeBG(JPanel parent) {
		setForeground(Color.BLACK);
		myParent = parent;
		this.setBounds(0, 0, Config.widthPageFunc, Config.heightPageFunc);
		setLayout(null);

		JLabel lblNewLabel = new JLabel("Một số background đã dùng");
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblNewLabel.setBounds(10, 11, 300, 33);
		add(lblNewLabel);

		panel = new JPanel();
		panel.setBackground(Color.WHITE);
		panel.setBounds(10, 55, 615, 225);
		add(panel);
		panel.setLayout(null);

		updateListRecentBG();

		panelReImage = new JPanel();
		panelReImage.setBackground(Color.WHITE);
		panelReImage.setBounds(635, 55, 549, 532);
		add(panelReImage);
		panelReImage.setLayout(null);
		///////////////////////////////////////////

		JLabel label = new JLabel("Thay đổi background cho desktop");
		label.setFont(new Font("Tahoma", Font.PLAIN, 16));
		label.setBounds(10, 291, 300, 33);
		add(label);

		JLabel lblFile = new JLabel("Tên file");
		lblFile.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblFile.setBounds(10, 392, 125, 33);
		add(lblFile);

		JButton btnChooseFile = new JButton("Chọn file");
		btnChooseFile.setBounds(10, 348, 125, 33);
		add(btnChooseFile);

		JButton btnChange = new JButton("Thay đổi");
		btnChange.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (chooseFile == null)
					return;
				new Changer().changeBackgroundDesktop(chooseFile.getAbsolutePath());
				InfoSystem.listRecentBG.add(0, chooseFile.getAbsolutePath());
				updateListRecentBG();
			}
		});
		btnChange.setBounds(10, 436, 125, 33);
		add(btnChange);

		lblNameFile = new JLabel((chooseFile == null) ? "dxtruong" : chooseFile.getAbsolutePath());
		lblNameFile.setForeground(Color.BLUE);
		lblNameFile.setBackground(Color.WHITE);
		lblNameFile.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblNameFile.setBounds(151, 392, 474, 33);
		add(lblNameFile);

		// action
		btnChooseFile.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JFileChooser fileChooser = new JFileChooser();
				String pathRoot = InfoSystem.chooseFolderBG;
				if(pathRoot == null) {
					pathRoot = System.getProperty("user.home");
				}
				fileChooser.setCurrentDirectory(new File(pathRoot));
				int result = fileChooser.showOpenDialog(myParent);
				if (result == JFileChooser.APPROVE_OPTION) {
					chooseFile = fileChooser.getSelectedFile();
					String name = chooseFile.getName();
					if(name.endsWith("jpg") | name.endsWith("png")|name.endsWith("jpeg")) {
						updateImage(panelReImage, chooseFile);
						lblNameFile.setText(chooseFile.getName());
					}else {
						JOptionPane.showMessageDialog(null, "dmm");
					}
				}
			}
		});
	}

	private void updateImage(JPanel panelImage, File chooseFile) {
		BufferedImage myPicture;
		panelImage.removeAll();
		try {
			myPicture = ImageIO.read(new File(chooseFile.getAbsolutePath()));
			JLabel lblImage = new JLabel(new ImageIcon(myPicture));
			lblImage.setPreferredSize(new Dimension(myPicture.getWidth(),myPicture.getHeight()));
			JScrollPane scroll = new JScrollPane(lblImage);
			scroll.setBounds(10, 10, panelImage.getWidth()-20, panelImage.getHeight()-20);
			panelImage.add(scroll);
			panelImage.revalidate();
			panelImage.repaint();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void updateListRecentBG() {
		int count = 0;
		panel.removeAll();
		for (String path : InfoSystem.listRecentBG) {
			if (count == 0) {
				btnImageOne = new JButton(path);
				btnImageOne.setHorizontalAlignment(SwingConstants.LEFT);
				btnImageOne.setBounds(10, 11, 595, 29);
				btnImageOne.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						updateImage(panelReImage, new File(btnImageOne.getText()));
						new Changer().changeBackgroundDesktop(btnImageOne.getText());
					}
				});
				panel.add(btnImageOne);
			} else if (count == 1) {
				btnImageTwo = new JButton(path);
				btnImageTwo.setHorizontalAlignment(SwingConstants.LEFT);
				btnImageTwo.setBounds(10, 51, 595, 29);
				btnImageTwo.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						updateImage(panelReImage, new File(btnImageTwo.getText()));
						new Changer().changeBackgroundDesktop(btnImageTwo.getText());
					}
				});
				panel.add(btnImageTwo);
			} else if (count == 2) {
				btnImageThree = new JButton(path);
				btnImageThree.setHorizontalAlignment(SwingConstants.LEFT);
				btnImageThree.setBounds(10, 91, 595, 29);
				btnImageThree.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						updateImage(panelReImage, new File(btnImageThree.getText()));
						new Changer().changeBackgroundDesktop(btnImageThree.getText());
					}
				});
				panel.add(btnImageThree);
			} else if (count == 3) {
				btnImageFour = new JButton(path);
				btnImageFour.setHorizontalAlignment(SwingConstants.LEFT);
				btnImageFour.setBounds(10, 131, 595, 29);
				btnImageFour.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						updateImage(panelReImage, new File(btnImageFour.getText()));
						new Changer().changeBackgroundDesktop(btnImageFour.getText());
					}
				});
				panel.add(btnImageFour);
			} else if (count == 4) {
				btnImageFive = new JButton(path);
				btnImageFive.setHorizontalAlignment(SwingConstants.LEFT);
				btnImageFive.setBounds(10, 171, 595, 29);
				btnImageFive.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						updateImage(panelReImage, new File(btnImageFive.getText()));
						new Changer().changeBackgroundDesktop(btnImageFive.getText());
					}
				});
				panel.add(btnImageFive);
			} else {
				break;
			}
			count++;
		}
		panel.revalidate();
		panel.repaint();
	}
}
