package com.app.view;

import javax.swing.JPanel;

import com.app.InfoSystem;
import com.app.common.Tool;
import com.app.config.Config;
import com.app.controller.alarm.CAlarm;
import com.app.model.Alarm;

import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import javax.swing.JTextField;
import com.toedter.calendar.JMonthChooser;
import com.toedter.calendar.JDayChooser;
import com.toedter.components.JLocaleChooser;
import com.toedter.calendar.JDateChooser;
import com.toedter.calendar.JCalendar;
import com.toedter.components.JSpinField;
import com.toedter.calendar.JYearChooser;
import javax.swing.JComboBox;
import javax.swing.JTextArea;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JEditorPane;
import javax.swing.JRadioButton;
import javax.swing.ComboBoxModel;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import java.awt.Color;
import java.awt.Dimension;

import javax.swing.JTextPane;
import javax.swing.JSpinner;
import javax.swing.JTable;
import javax.swing.SpinnerNumberModel;
import java.awt.event.ItemListener;
import java.awt.event.ItemEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ScheduledFuture;
import java.beans.PropertyChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.ChangeEvent;
import javax.swing.table.DefaultTableModel;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.ScrollPaneConstants;

public class VAlarm extends JPanel {

	/**
	 * Create the panel.
	 */
	private JPanel myParent, panelTable;
	private JComboBox cbType, cbTypeLoop, cbTypeLoopMonth;
	private JTextArea textContent;
	private JRadioButton radMon, radTue, radWed, radThu, radFri, radSat, radSun, radSelectionOne, radSelectionTwo,
			radSelectionThree;
	private JButton btnCreate;
	private JLabel label, lblLoop, lblLoopweek, lblLoopmonth, lblEnd;
	private JLabel label_1;
	private JSpinner spinner;
	private JSpinner spHour;
	private JSpinner spMin, spEnd;
	private JDateChooser dateEnd, dateExpire;
	private JScrollPane scrollTable;
	private JTable tableAlarm;
	private Alarm alarmFocus;
	private List<Alarm> alarms;
	private int saveMode = 0;
	private JTextPane textPane,note;
	private JButton btnCancel, btnActive;
	String column[] = new String[] { "STT", "Thời gian báo", "Chế độ lặp", "Thời gian kết thúc", "Trạng thái" };

	public VAlarm(JPanel parent) {
		alarms = new CAlarm().getAlarms();
		InfoSystem.als = alarms;
		myParent = parent;
		this.setBounds(0, 0, Config.widthPageFunc, Config.heightPageFunc);
		setLayout(null);

		JLabel lblContent = new JLabel("Nội dung");
		lblContent.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblContent.setBounds(10, 11, 84, 27);
		add(lblContent);

		JLabel lblTime = new JLabel("Thời gian");
		lblTime.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblTime.setBounds(10, 109, 84, 27);
		add(lblTime);

		dateExpire = new JDateChooser();
		dateExpire.setBounds(104, 105, 205, 31);
		dateExpire.setDate(new Date());
		add(dateExpire);

		JLabel lblType = new JLabel("Lặp lại");
		lblType.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblType.setBounds(10, 158, 84, 27);
		add(lblType);

		String[] modecbType = new String[] { "Không lặp lại", "Hàng ngày", "Hàng tuần", "Hàng tháng", "Hàng năm",
				"Tùy chỉnh" };
		cbType = new JComboBox(modecbType);
		cbType.setFont(new Font("Tahoma", Font.PLAIN, 12));
		cbType.setBounds(104, 156, 446, 30);
		cbType.setSelectedIndex(0);
		add(cbType);

		lblLoop = new JLabel("Lặp sau mỗi");
		lblLoop.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblLoop.setBounds(10, 212, 84, 27);
		add(lblLoop);

		textContent = new JTextArea();
		textContent.setFont(new Font("Tahoma", Font.PLAIN, 12));
		textContent.setBounds(104, 13, 446, 81);
		add(textContent);

		String modeLoop[] = new String[] { "ngày", "tuần", "tháng", "năm" };
		cbTypeLoop = new JComboBox(modeLoop);
		cbTypeLoop.setBounds(182, 213, 368, 27);
		cbTypeLoop.setSelectedIndex(0);
		add(cbTypeLoop);

		lblLoopweek = new JLabel("Lặp theo tuần");
		lblLoopweek.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblLoopweek.setBounds(10, 262, 84, 27);
		add(lblLoopweek);

		radMon = new JRadioButton("Thứ Hai");
		radMon.setFont(new Font("Tahoma", Font.PLAIN, 12));
		radMon.setBounds(104, 262, 93, 27);
		add(radMon);

		radTue = new JRadioButton("Thứ Ba");
		radTue.setFont(new Font("Tahoma", Font.PLAIN, 12));
		radTue.setBounds(216, 262, 93, 27);
		add(radTue);

		radWed = new JRadioButton("Thứ Tư");
		radWed.setFont(new Font("Tahoma", Font.PLAIN, 12));
		radWed.setBounds(331, 262, 93, 27);
		add(radWed);

		radFri = new JRadioButton("Thứ Sáu");
		radFri.setFont(new Font("Tahoma", Font.PLAIN, 12));
		radFri.setBounds(104, 309, 93, 27);
		add(radFri);

		radSat = new JRadioButton("Thứ Bảy");
		radSat.setFont(new Font("Tahoma", Font.PLAIN, 12));
		radSat.setBounds(216, 309, 93, 27);
		add(radSat);

		radSun = new JRadioButton("Chủ Nhật");
		radSun.setFont(new Font("Tahoma", Font.PLAIN, 12));
		radSun.setBounds(331, 309, 93, 27);
		add(radSun);

		radThu = new JRadioButton("Thứ Năm");
		radThu.setFont(new Font("Tahoma", Font.PLAIN, 12));
		radThu.setBounds(441, 262, 109, 27);
		add(radThu);

		lblLoopmonth = new JLabel("Lặp theo tháng");
		lblLoopmonth.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblLoopmonth.setBounds(10, 357, 123, 27);
		add(lblLoopmonth);

		cbTypeLoopMonth = new JComboBox(new String[] {"a","b","c"});
		cbTypeLoopMonth.setSelectedIndex(0);
		cbTypeLoopMonth.setBounds(104, 358, 446, 27);
		add(cbTypeLoopMonth);

		lblEnd = new JLabel("Kết thúc");
		lblEnd.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblEnd.setBounds(10, 406, 84, 27);
		add(lblEnd);

		radSelectionOne = new JRadioButton("Không bao giờ");
		radSelectionOne.setSelected(true);
		radSelectionOne.setFont(new Font("Tahoma", Font.PLAIN, 12));
		radSelectionOne.setBounds(104, 406, 129, 27);
		add(radSelectionOne);

		radSelectionTwo = new JRadioButton("Vào ngày");
		radSelectionTwo.setFont(new Font("Tahoma", Font.PLAIN, 12));
		radSelectionTwo.setBounds(104, 448, 129, 27);
		add(radSelectionTwo);

		radSelectionThree = new JRadioButton("Sau số lần");
		radSelectionThree.setFont(new Font("Tahoma", Font.PLAIN, 12));
		radSelectionThree.setBounds(104, 492, 129, 27);
		add(radSelectionThree);

		JButton btnSave = new JButton("Lưu");
		btnSave.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				functionSave();
				Tool.writeAlarm(alarms);
			}
		});
		btnSave.setFont(new Font("Tahoma", Font.PLAIN, 12));
		btnSave.setBounds(441, 538, 109, 31);
		add(btnSave);

		JPanel panel = new JPanel();
		panel.setBackground(Color.MAGENTA);
		panel.setForeground(Color.BLACK);
		panel.setBounds(560, 11, 2, 558);
		add(panel);

		panelTable = new JPanel();
		panelTable.setBackground(Color.WHITE);
		panelTable.setBounds(572, 11, 612, 370);
		add(panelTable);
		panelTable.setLayout(null);

		btnCancel = new JButton("Tắt");
		btnCancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				functionTurnOff(alarmFocus);
			}
		});
		btnCancel.setFont(new Font("Tahoma", Font.PLAIN, 12));
		btnCancel.setBounds(837, 392, 109, 31);
		add(btnCancel);

		btnActive = new JButton("Bật");
		btnActive.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				functionTurnOn(alarmFocus);
			}
		});
		btnActive.setFont(new Font("Tahoma", Font.PLAIN, 12));
		btnActive.setBounds(956, 392, 109, 31);
		add(btnActive);

		JButton btnDelete = new JButton("Xóa");
		btnDelete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				functionDelete();
				resetPage();
			}
		});
		btnDelete.setFont(new Font("Tahoma", Font.PLAIN, 12));
		btnDelete.setBounds(1075, 392, 109, 31);
		add(btnDelete);

		textPane = new JTextPane();
		textPane.setBackground(Color.YELLOW);
		textPane.setForeground(Color.RED);
		textPane.setFont(new Font("Tahoma", Font.PLAIN, 12));
		textPane.setBounds(572, 434, 612, 135);
		add(textPane);

		btnCreate = new JButton("Tạo mới");
		btnCreate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				functionCreate();
			}
		});
		btnCreate.setFont(new Font("Tahoma", Font.PLAIN, 12));
		btnCreate.setBounds(734, 392, 93, 31);
		add(btnCreate);

		label = new JLabel("Giờ");
		label.setFont(new Font("Tahoma", Font.PLAIN, 12));
		label.setBounds(319, 105, 32, 31);
		add(label);

		label_1 = new JLabel("Phút");
		label_1.setFont(new Font("Tahoma", Font.PLAIN, 12));
		label_1.setBounds(441, 105, 38, 31);
		add(label_1);

		spinner = new JSpinner();
		spinner.setFont(new Font("Tahoma", Font.PLAIN, 12));
		spinner.setModel(new SpinnerNumberModel(1, 1, 100, 1));
		spinner.setBounds(104, 213, 68, 27);
		add(spinner);

		spHour = new JSpinner();
		spHour.setFont(new Font("Tahoma", Font.PLAIN, 12));
		spHour.setModel(new SpinnerNumberModel(0, 0, 23, 1));
		spHour.setBounds(361, 105, 68, 31);
		add(spHour);

		spMin = new JSpinner();
		spMin.setFont(new Font("Tahoma", Font.PLAIN, 12));
		spMin.setModel(new SpinnerNumberModel(0, 0, 59, 1));
		spMin.setBounds(482, 105, 68, 31);
		add(spMin);

		dateEnd = new JDateChooser();
		dateEnd.setDate(new Date());
		dateEnd.setBounds(264, 448, 177, 27);
		add(dateEnd);

		spEnd = new JSpinner();
		spEnd.setModel(new SpinnerNumberModel(new Integer(1), new Integer(1), null, new Integer(1)));
		spEnd.setFont(new Font("Tahoma", Font.PLAIN, 12));
		spEnd.setBounds(263, 486, 68, 31);
		add(spEnd);

		Object data[][] = new CAlarm().getInfoAlarms(alarms);
		// String data [][] = new String [][] {{"1","9:30 22/10/2020","Không lặp
		// lại","Sau một lần","Đã đặt"},{"2","10:30 23/12/2020","Hàng năm","Sau 20 lần
		// báo","Hủy đặt"}};
		DefaultTableModel modeTable = new DefaultTableModel(data, column);
		tableAlarm = new JTable(modeTable) {
			public boolean editCellAt(int row, int column, java.util.EventObject e) {
				return false;
			}
		};
		tableAlarm.setRowSelectionAllowed(true);
		tableAlarm.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				functionChooseRow();
			}
		});
		tableAlarm.setFont(new Font("Tahoma", Font.PLAIN, 12));
//		tableAlarm.getColumnModel().getColumn(0).setPreferredWidth(50);
//		tableAlarm.getColumnModel().getColumn(0).setMinWidth(50);
//		tableAlarm.getColumnModel().getColumn(1).setPreferredWidth(125);
//		tableAlarm.getColumnModel().getColumn(1).setMinWidth(125);
//		tableAlarm.getColumnModel().getColumn(2).setPreferredWidth(275);
//		tableAlarm.getColumnModel().getColumn(2).setMinWidth(275);
//		tableAlarm.getColumnModel().getColumn(3).setPreferredWidth(200);
//		tableAlarm.getColumnModel().getColumn(3).setMinWidth(200);
//		tableAlarm.getColumnModel().getColumn(4).setPreferredWidth(100);
//		tableAlarm.getColumnModel().getColumn(4).setMinWidth(100);
		tableAlarm.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
		tableAlarm.setRowHeight(25);
		scrollTable = new JScrollPane(tableAlarm);
		scrollTable.setBounds(0, 0, panelTable.getWidth(), panelTable.getHeight());
		panelTable.add(scrollTable);
		
		note = new JTextPane();
		note.setText("Đang tạo dữ liệu");
		note.setBackground(Color.GREEN);
		note.setFont(new Font("Tahoma", Font.PLAIN, 12));
		note.setBounds(10, 538, 191, 27);
		add(note);

		///////////////////////////////
		resetPage();
		///
		cbType.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				setupTypeAlarm();
			}
		});

		cbTypeLoop.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				setupCBTypeLoop();
			}
		});

		radSelectionOne.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent evt) {
				if (radSelectionOne.isSelected() == true) {
					radSelectionTwo.setSelected(false);
					radSelectionThree.setSelected(false);
				}
			}
		});
		radSelectionTwo.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent evt) {
				if (radSelectionTwo.isSelected() == true) {
					radSelectionOne.setSelected(false);
					radSelectionThree.setSelected(false);
					dateEnd.setEnabled(true);
				} else {
					dateEnd.setEnabled(false);
				}
			}
		});
		radSelectionThree.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent evt) {
				if (radSelectionThree.isSelected() == true) {
					radSelectionOne.setSelected(false);
					radSelectionTwo.setSelected(false);
					spEnd.setEnabled(true);
				} else {
					spEnd.setEnabled(false);
				}
			}
		});
	}

	protected void setupTypeAlarm() {
		int index = cbType.getSelectedIndex();
		if (index == 5) {
			lblLoop.setVisible(true);
			lblLoopweek.setVisible(false);
			lblLoopmonth.setVisible(false);
			lblEnd.setVisible(true);
			cbTypeLoop.setVisible(true);
			spinner.setVisible(true);
			radMon.setVisible(false);
			radTue.setVisible(false);
			radWed.setVisible(false);
			radThu.setVisible(false);
			radFri.setVisible(false);
			radSat.setVisible(false);
			radSun.setVisible(false);
			radSelectionOne.setVisible(true);
			radSelectionTwo.setVisible(true);
			radSelectionThree.setVisible(true);
			spEnd.setVisible(true);
			dateEnd.setVisible(true);
			cbTypeLoopMonth.setVisible(false);
			setupCBTypeLoop();
		} else {
			lblLoop.setVisible(false);
			lblLoopweek.setVisible(false);
			lblLoopmonth.setVisible(false);
			lblEnd.setVisible(false);
			cbTypeLoop.setVisible(false);
			spinner.setVisible(false);
			radMon.setVisible(false);
			radTue.setVisible(false);
			radWed.setVisible(false);
			radThu.setVisible(false);
			radFri.setVisible(false);
			radSat.setVisible(false);
			radSun.setVisible(false);
			radSelectionOne.setVisible(false);
			radSelectionTwo.setVisible(false);
			radSelectionThree.setVisible(false);
			spEnd.setVisible(false);
			dateEnd.setVisible(false);
			cbTypeLoopMonth.setVisible(false);
		}
	}

	protected void setModelMonthOnDate() {
		Calendar ca = Calendar.getInstance();
		ca.setTime(dateExpire.getDate());
		String a = "Lặp vào ngày " + ca.get(Calendar.DAY_OF_MONTH) + " trong tháng";
		String b = "Lặp vào " + getThu(ca.get(Calendar.DAY_OF_WEEK)) + " của tuần thứ " + ca.get(Calendar.WEEK_OF_MONTH)
				+ " trong tháng";
		String c = "Lặp vào " + getThu(ca.get(Calendar.DAY_OF_WEEK)) + " thứ " + ca.get(Calendar.DAY_OF_WEEK_IN_MONTH)
				+ " trong tháng";
		String model[] = new String[] { a, b, c };
		cbTypeLoopMonth.setModel(new DefaultComboBoxModel(model));
		cbTypeLoopMonth.setSelectedIndex(0);
	}

	protected void functionChooseRow() {
		int index = tableAlarm.getSelectedRow();
		if (index == -1) {
			return;
		}
		int id = (int) tableAlarm.getModel().getValueAt(index, 0);
		alarmFocus = alarms.get(id - 1);
		textPane.setText(alarmFocus.getMes());
		if(alarmFocus.getStatus()==0) {
			btnActive.setEnabled(false);
			btnCancel.setEnabled(true);
		}else {
			btnActive.setEnabled(true);
			btnCancel.setEnabled(false);
		}
		functionView();
	}

	protected void functionCreate() {
		note.setText("Đang tạo mới");
		saveMode = 1;
		resetPage();
	}

	protected void functionView() {
		note.setText("Đang cập nhật");
		saveMode = 2;
		resetPage();
		if (alarmFocus != null) {
			textContent.setText(alarmFocus.getMes());
			Calendar ca = Calendar.getInstance();
			ca.setTimeInMillis(alarmFocus.getTime());
			dateExpire.setDate(ca.getTime());
			spHour.setValue(ca.get(Calendar.HOUR_OF_DAY));
			spMin.setValue(ca.get(Calendar.MINUTE));
			cbType.setSelectedIndex(alarmFocus.getType());
			spinner.setValue(alarmFocus.getTimeLoop());
			cbTypeLoop.setSelectedIndex(alarmFocus.getTypeLoop());
			radMon.setSelected(false);
			radTue.setSelected(false);
			radWed.setSelected(false);
			radThu.setSelected(false);
			radFri.setSelected(false);
			radSat.setSelected(false);
			radSun.setSelected(false);
			for (int i : alarmFocus.getDateLoop()) {
				if (i == 0) {
					radMon.setSelected(true);
				} else if (i == 1) {
					radTue.setSelected(true);
				} else if (i == 2) {
					radWed.setSelected(true);
				} else if (i == 3) {
					radThu.setSelected(true);
				} else if (i == 4) {
					radFri.setSelected(true);
				} else if (i == 5) {
					radSat.setSelected(true);
				} else if (i == 6) {
					radSun.setSelected(true);
				}
			}
			setModelMonthOnDate();
			cbTypeLoopMonth.setSelectedIndex(alarmFocus.getTypeLoopByMonth());
			dateEnd.setDate(new Date(alarmFocus.getTimeFinish()));
			spEnd.setValue(alarmFocus.getTimeLimitExe());
			if (alarmFocus.getTypeEnd() == 1) {
				radSelectionOne.setSelected(true);
			} else if (alarmFocus.getTypeEnd() == 2) {
				radSelectionTwo.setSelected(true);
			} else {
				radSelectionThree.setSelected(true);
			}
		}
	}

	protected void functionDelete() {
		if(tableAlarm.getSelectedRow()>0) {
			List<Alarm> deletes = new ArrayList<Alarm>();
			int [] ids = tableAlarm.getSelectedRows();
			for(int i : ids) {
				Alarm a = alarms.get((int)tableAlarm.getValueAt(i, 0)-1);
				if(a.getStatus() == 0) {
					Map<String,ScheduledFuture<?>> map = InfoSystem.taskIds;
					if(map.containsKey(alarmFocus.getUid().toString())) {
						map.get(alarmFocus.getUid().toString()).cancel(true);
						map.remove(alarmFocus.getUid().toString());
						map.get(alarmFocus.getUid().toString()+"music").cancel(true);
						map.remove(alarmFocus.getUid().toString()+"music");
					}
				}
				deletes.add(a);
			}
			for(Alarm b : deletes) {
				boolean d = alarms.remove(b);
				System.out.println(d);
			}
			displayDataTable();
			Tool.writeAlarm(alarms);
		}else {
			JOptionPane.showMessageDialog(null, "Mày chưa chọn dòng cái focus đấy vô dụng nhé :))");
		}
	}

	protected void functionTurnOn(Alarm a) {
		if(a == null) {
			JOptionPane.showMessageDialog(null, "Mày chưa chọn dòng cái focus đấy vô dụng nhé :))");
			return;
		}
		Map<String,ScheduledFuture<?>> map = InfoSystem.taskIds;
		a.setTimeExe(0);
		a.setStatus(0);
		if(!map.containsKey(a.getUid().toString())) {
			Tool.turnOnAlarm(a);
			System.out.println("turn on");
		}
		new CAlarm().saveAlarm(a, tableAlarm);
		btnActive.setEnabled(false);
		btnCancel.setEnabled(true);
		Tool.writeAlarm(alarms);
	}

	protected void functionTurnOff(Alarm a) {
		if(a == null) {
			JOptionPane.showMessageDialog(null, "Mày chưa chọn dòng cái focus đấy vô dụng nhé :))");
			return;
		}
		Map<String,ScheduledFuture<?>> map = InfoSystem.taskIds;
		if(map.containsKey(a.getUid().toString())) {
			map.get(a.getUid().toString()).cancel(true);
			map.remove(a.getUid().toString());
			map.get(a.getUid().toString()+"music").cancel(true);
			map.remove(a.getUid().toString()+"music");
		}
		a.setStatus(1);
		new CAlarm().saveAlarm(a, tableAlarm);
		btnActive.setEnabled(true);
		btnCancel.setEnabled(false);
		Tool.writeAlarm(alarms);
	}

	protected void functionSave() {
		if (alarmFocus == null & saveMode == 2) {
			JOptionPane.showMessageDialog(null, "Mày chưa chọn dòng cái focus đấy vô dụng nhé :))");
			return;
		}
		alarmFocus = createObjectAlarm();
		if(saveMode == 2) {
			new CAlarm().saveAlarm(alarmFocus, tableAlarm);
		}else {
			new CAlarm().addAlarm(alarmFocus, tableAlarm);
		}
		//displayDataTable();
		alarmFocus = null;
		note.setText("Đang tạo dữ liệu");
		Tool.writeAlarm(alarms);
	}

	private void resetPage() {
		textContent.setText("");
		Calendar ca = Calendar.getInstance();
		dateExpire.setDate(ca.getTime());
		spHour.setValue(ca.get(Calendar.HOUR_OF_DAY));
		spMin.setValue(ca.get(Calendar.MINUTE));
		cbType.setSelectedIndex(0);
		lblLoop.setVisible(false);
		lblLoopweek.setVisible(false);
		lblLoopmonth.setVisible(false);
		lblEnd.setVisible(false);
		cbTypeLoop.setVisible(false);
		spinner.setVisible(false);
		radMon.setVisible(false);
		radTue.setVisible(false);
		radWed.setVisible(false);
		radThu.setVisible(false);
		radFri.setVisible(false);
		radSat.setVisible(false);
		radSun.setVisible(false);
		radSelectionOne.setVisible(false);
		radSelectionTwo.setVisible(false);
		radSelectionThree.setVisible(false);
		spEnd.setVisible(false);
		dateEnd.setVisible(false);
		cbTypeLoopMonth.setVisible(false);
		dateEnd.setEnabled(false);
		spEnd.setEnabled(false);
	}

	private void displayDataTable() {
		if (alarms.size() >= 0) {
			Object data[][] = new CAlarm().getInfoAlarms(alarms);
			DefaultTableModel model = (DefaultTableModel) tableAlarm.getModel();
			model.setDataVector(data, column);
			
		}
	}

	private String getThu(int x) {
		switch (x) {
		case 1:
			return "Chủ nhật";
		case 2:
			return "Thứ hai";
		case 3:
			return "Thứ ba";
		case 4:
			return "Thứ tư";
		case 5:
			return "Thứ năm";
		case 6:
			return "Thứ sáu";
		case 7:
			return "Thứ bảy";
		default:
			return "Thứ lozz";
		}
	}

	private void setupCBTypeLoop() {
		int index = cbTypeLoop.getSelectedIndex();
		if (cbType.getSelectedIndex() == 5) {
			if (index == 1) {
				lblLoopweek.setVisible(true);
				lblLoopmonth.setVisible(false);
				radMon.setVisible(true);
				radTue.setVisible(true);
				radWed.setVisible(true);
				radThu.setVisible(true);
				radFri.setVisible(true);
				radSat.setVisible(true);
				radSun.setVisible(true);
				cbTypeLoopMonth.setVisible(false);
			} else if (index == 2) {
				lblLoopweek.setVisible(false);
				lblLoopmonth.setVisible(true);
				radMon.setVisible(false);
				radTue.setVisible(false);
				radWed.setVisible(false);
				radThu.setVisible(false);
				radFri.setVisible(false);
				radSat.setVisible(false);
				radSun.setVisible(false);
				cbTypeLoopMonth.setVisible(true);

				setModelMonthOnDate();
			} else {
				lblLoopweek.setVisible(false);
				lblLoopmonth.setVisible(false);
				radMon.setVisible(false);
				radTue.setVisible(false);
				radWed.setVisible(false);
				radThu.setVisible(false);
				radFri.setVisible(false);
				radSat.setVisible(false);
				radSun.setVisible(false);
				cbTypeLoopMonth.setVisible(false);
			}
		}
	}

	private Alarm createObjectAlarm() {
		Alarm a = null;
		if (saveMode == 1 | saveMode == 0) {// create
			a = new Alarm();
			a.setId(tableAlarm.getRowCount()+1);
			alarms.add(a);
		} else {
			a = alarmFocus;
		}
		a.setMes(textContent.getText());
		Calendar ca = Calendar.getInstance();
		ca.setTime(dateExpire.getDate());
		ca.set(Calendar.HOUR_OF_DAY, (int) spHour.getValue());
		ca.set(Calendar.MINUTE, (int) spMin.getValue());
		a.setTime(ca.getTimeInMillis());
		List<Integer> dm = new ArrayList<Integer>();
		if (radMon.isSelected()) {
			dm.add(0);
		}
		if (radTue.isSelected()) {
			dm.add(1);
		}
		if (radWed.isSelected()) {
			dm.add(2);
		}
		if (radThu.isSelected()) {
			dm.add(3);
		}
		if (radFri.isSelected()) {
			dm.add(4);
		}
		if (radSat.isSelected()) {
			dm.add(5);
		}
		if (radSun.isSelected()) {
			dm.add(6);
		}
		int[] dc = new int[dm.size()];
		for (int i = 0; i < dm.size(); i++) {
			dc[i] = dm.get(i);
		}
		a.setDateLoop(dc);
		a.setStatus(1);
		a.setTimeExe(0);
		a.setTimeFinish(dateEnd.getDate().getTime());
		a.setTimeLimitExe((int) spEnd.getValue());
		a.setTimeLoop((int) spinner.getValue());
		a.setType(cbType.getSelectedIndex());
		a.setTypeLoop(cbTypeLoop.getSelectedIndex());
		a.setTypeLoopByMonth(cbTypeLoopMonth.getSelectedIndex());
		if (radSelectionOne.isSelected()) {
			a.setTypeEnd(1);
		} else if (radSelectionTwo.isSelected()) {
			a.setTypeEnd(2);
		} else if (radSelectionThree.isSelected()) {
			a.setTypeEnd(3);
		}
		return a;
	}
}
