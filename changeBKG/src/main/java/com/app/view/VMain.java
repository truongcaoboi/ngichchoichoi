package com.app.view;

import javax.swing.JPanel;

import com.app.config.Config;
import com.app.controller.manager.Navigation;

import javax.swing.border.LineBorder;
import java.awt.Color;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.border.EmptyBorder;
import javax.swing.border.MatteBorder;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class VMain extends JPanel {

	/**
	 * Create the panel.
	 */
	private JPanel myParent;
	private int tab = Config.tabWhether;
	public JPanel viewFunc;
	public VMain(JPanel parent) {
		myParent = parent;
		setBorder(new LineBorder(new Color(0, 0, 0)));
		this.setBounds(0, 0, Config.widthPageMain, Config.heightPageMain);
		setLayout(null);
		setVisible(true);
		
		JButton btnSetting = new JButton("Cài đặt");
		btnSetting.setBounds(1099, 11, 105, 34);
		add(btnSetting);
		
		JButton btnWhether = new JButton("Thời tiết");
		btnWhether.setBounds(46, 11, 105, 34);
		add(btnWhether);
		
		JButton btnChangerBG = new JButton("Hình nền desktop");
		btnChangerBG.setBounds(173, 11, 151, 34);
		add(btnChangerBG);
		
		JButton btnAlarm = new JButton("Hẹn giờ");
		btnAlarm.setBounds(355, 11, 105, 34);
		add(btnAlarm);
		
		JButton btnManager = new JButton("File");
		btnManager.setBounds(485, 11, 105, 34);
		add(btnManager);
		
		viewFunc = new JPanel();
		viewFunc.setBorder(new MatteBorder(1, 1, 1, 1, (Color) new Color(0, 0, 0)));
		viewFunc.setBounds(10, 58, 1194, 598);
		add(viewFunc);
		viewFunc.setLayout(null);
		
		new Navigation().goToWhetherPage(viewFunc);
		
		JPanel panel = new JPanel();
		panel.setBackground(Color.BLACK);
		panel.setBounds(10, 50, 1194, 5);
		add(panel);
		
		//action button
		btnWhether.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(tab == Config.tabWhether) return;
				new Navigation().goToWhetherPage(viewFunc);
				tab = Config.tabWhether;
			}
		});
		
		btnChangerBG.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(tab == Config.tabChanger) return;
				new Navigation().goToChageBGDesktop(viewFunc);
				tab = Config.tabChanger;
			}
		});
		
		btnAlarm.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(tab == Config.tabAlarm) return;
				new Navigation().goToAlarmPage(viewFunc);
				tab = Config.tabAlarm;
			}
		});
		
		btnManager.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(tab == Config.tabManager) return;
				new Navigation().goToManagerPage(viewFunc);
				tab = Config.tabManager;
			}
		});
		
		btnSetting.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new Navigation().goToSettingPage(myParent);
			}
		});
	}
}
