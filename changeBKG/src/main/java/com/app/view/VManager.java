package com.app.view;

import javax.swing.JPanel;
import javax.swing.JScrollPane;

import com.app.InfoSystem;
import com.app.config.Config;

import javafx.scene.image.Image;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JLabel;

import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.awt.event.ActionEvent;
import java.awt.Color;
import java.awt.Dimension;

public class VManager extends JPanel {

	/**
	 * Create the panel.
	 */
	private JPanel myParent, panel;
	private int index = 0;

	public VManager(JPanel parent) {
		myParent = parent;
		this.setBounds(0, 0, Config.widthPageFunc, Config.heightPageFunc);
		setLayout(null);

		JButton btnSoHot = new JButton("Nắng nóng");
		btnSoHot.setBounds(10, 11, 115, 30);
		add(btnSoHot);

		JButton btnSun = new JButton("Nắng nhẹ");
		btnSun.setBounds(135, 11, 115, 30);
		add(btnSun);

		JButton btnCloudy = new JButton("Có mây");
		btnCloudy.setBounds(259, 11, 115, 30);
		add(btnCloudy);

		JButton btnSoCloudy = new JButton("Nhiều mây");
		btnSoCloudy.setBounds(384, 11, 115, 30);
		add(btnSoCloudy);

		JButton btnGiong = new JButton("Giông bão");
		btnGiong.setBounds(507, 11, 115, 30);
		add(btnGiong);

		JButton btnRain = new JButton("Mưa");
		btnRain.setBounds(632, 11, 115, 30);
		add(btnRain);

		JButton btnCool = new JButton("Mát mẻ");
		btnCool.setBounds(757, 11, 115, 30);
		add(btnCool);

		JButton btnCold = new JButton("Lạnh");
		btnCold.setBounds(884, 11, 115, 30);
		add(btnCold);

		JButton btnOther = new JButton("Khác");
		btnOther.setBounds(1009, 11, 115, 30);
		add(btnOther);

		panel = new JPanel();
		panel.setBackground(Color.WHITE);
		panel.setBounds(10, 52, 1174, 491);
		add(panel);

		JButton btnUpdate = new JButton("Thay đổi");
		btnUpdate.setBounds(10, 554, 115, 30);
		add(btnUpdate);

		JButton btnDefault = new JButton("Khôi phục mặc định");
		btnDefault.setBounds(135, 554, 153, 30);
		add(btnDefault);

		////
		btnSoHot.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setPicture(InfoSystem.pathSun);
				index = 0;
			}
		});
		btnSun.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setPicture(InfoSystem.pathHaveSun);
				index = 1;
			}
		});
		btnCloudy.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setPicture(InfoSystem.pathSunCloud);
				index = 2;
			}
		});
		btnSoCloudy.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setPicture(InfoSystem.pathMuchCloud);
				index = 3;
			}
		});
		btnGiong.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setPicture(InfoSystem.pathGiongBao);
				index = 4;
			}
		});
		btnRain.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setPicture(InfoSystem.pathRain);
				index = 5;
			}
		});
		btnCool.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setPicture(InfoSystem.pathCool);
				index = 6;
			}
		});
		btnCold.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setPicture(InfoSystem.pathCold);
				index = 7;
			}
		});
		btnOther.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setPicture(InfoSystem.pathOther);
				index = 8;
			}
		});
		btnUpdate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JFileChooser chooser = new JFileChooser();
				String pathRoot = InfoSystem.chooseFolderBG;
				if (pathRoot == null) {
					pathRoot = System.getProperty("user.home");
				}
				chooser.setCurrentDirectory(new File(pathRoot));
				chooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
				int result = chooser.showOpenDialog(myParent);
				if (result == JFileChooser.APPROVE_OPTION) {
					File file = chooser.getSelectedFile();
					switch (index) {
					case 0:
						InfoSystem.pathSun = file.getAbsolutePath();
						break;
					case 1:
						InfoSystem.pathHaveSun = file.getAbsolutePath();
						break;
					case 2:
						InfoSystem.pathSunCloud = file.getAbsolutePath();
						break;
					case 3:
						InfoSystem.pathMuchCloud = file.getAbsolutePath();
						break;
					case 4:
						InfoSystem.pathGiongBao = file.getAbsolutePath();
						break;
					case 5:
						InfoSystem.pathRain = file.getAbsolutePath();
						break;
					case 6:
						InfoSystem.pathCool = file.getAbsolutePath();
						break;
					case 7:
						InfoSystem.pathCold = file.getAbsolutePath();
						break;
					case 8:
						InfoSystem.pathOther = file.getAbsolutePath();
						break;
					}
					setPicture(file.getAbsolutePath());
				}
			}
		});

		btnDefault.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				List<String> a = new ArrayList<String>();
				InfoSystem.pathSun = "image\\sun.jpg";
				a.add(InfoSystem.pathSun);
				InfoSystem.pathHaveSun = "image\\conang.jpg";
				a.add(InfoSystem.pathHaveSun);
				InfoSystem.pathSunCloud = "image\\cloudy.jpg";
				a.add(InfoSystem.pathSunCloud);
				InfoSystem.pathMuchCloud = "image\\much_cloudy.jpg";
				a.add(InfoSystem.pathMuchCloud);
				InfoSystem.pathCool = "image\\cool.jpg";
				a.add(InfoSystem.pathCool);
				InfoSystem.pathCold = "image\\cold.jpg";
				a.add(InfoSystem.pathCold);
				InfoSystem.pathGiongBao = "image\\giongbao.jpg";
				a.add(InfoSystem.pathGiongBao);
				InfoSystem.pathRain = "image\\rain.jpg";
				a.add(InfoSystem.pathRain);
				InfoSystem.pathOther = "image\\other.jpg";
				a.add(InfoSystem.pathOther);
				setPicture(a.get(index));
			}
		});
	}

	private void setPicture(String path) {
		BufferedImage picture;
		try {
			picture = ImageIO.read(new File(path));
			JLabel image = new JLabel(new ImageIcon(picture));
//			int width = (picture.getWidth() + 20 < panel.getWidth()) ? panel.getWidth() - 20 : picture.getWidth();
//			int height = (picture.getHeight() + 20 < panel.getHeight()) ? panel.getHeight() - 20 : picture.getHeight();
			image.setPreferredSize(new Dimension(picture.getWidth(), picture.getHeight()));
			JScrollPane scroll = new JScrollPane(image);
			scroll.setBounds(10, 10, panel.getWidth() - 20, panel.getHeight() - 20);
			panel.removeAll();
			panel.add(scroll);
			panel.revalidate();
			panel.repaint();
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
	}
}
