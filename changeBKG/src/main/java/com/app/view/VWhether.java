package com.app.view;

import javax.swing.JPanel;

import com.app.InfoSystem;
import com.app.common.Tool;
import com.app.config.Config;
import com.app.controller.whether.CWhether;
import com.app.model.Whether;
import com.app.model.WhetherOther;

import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import javax.swing.JButton;
import java.awt.Color;
import javax.swing.SwingConstants;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class VWhether extends JPanel {

	/**
	 * Create the panel.
	 */
	private JPanel myParent;
	private JPanel panel;
	public VWhether(JPanel parent) {
		Whether we = InfoSystem.we;
		if(we == null) {
			we = new CWhether().getWhether();
			InfoSystem.we = we;
			Tool.writeWhether();
		}
		myParent = parent;
		this.setBounds(0, 0, Config.widthPageFunc, Config.heightPageFunc);
		setLayout(null);
		setVisible(true);
		
		JLabel lblNewLabel = new JLabel("Thời tiết trong ngày");
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblNewLabel.setBounds(10, 17, 159, 25);
		add(lblNewLabel);
		
		JButton btnNewButton = new JButton("Cập nhật");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				synchronized (InfoSystem.we) {
					Whether we = new CWhether().getWhether();
					resetWhetherInfo(we);
					InfoSystem.we = we;
					Tool.writeWhether();
					JOptionPane.showMessageDialog(null, "Cập nhật thành công");
				}
			}
		});
		btnNewButton.setBounds(1095, 14, 89, 34);
		add(btnNewButton);
		
		panel = new JPanel();
		panel.setBackground(Color.WHITE);
		panel.setBounds(10, 59, 1174, 212);
		add(panel);
		panel.setLayout(null);
		resetWhetherInfo(we);
		
		JLabel label = new JLabel("Dự báo thời tiết các ngày tới");
		label.setFont(new Font("Tahoma", Font.PLAIN, 16));
		label.setBounds(10, 298, 258, 25);
		add(label);
		int x = 10;
		int count =0;
		if(we == null) {
			return;
		}
		for(WhetherOther w : we.getWhetherOthers()) {
			if(count == 0) {
				count++;
				continue;
			}
			JPanel panel_1 = new JPanel();
			panel_1.setBackground(Color.WHITE);
			panel_1.setBounds(x, 334, 187, 253);
			add(panel_1);
			panel_1.setLayout(null);
			
			JLabel label_1 = new JLabel(w.getTime());
			label_1.setHorizontalAlignment(SwingConstants.LEFT);
			label_1.setFont(new Font("Tahoma", Font.PLAIN, 14));
			label_1.setBounds(10, 11, 167, 31);
			panel_1.add(label_1);
			
			JLabel label_2 = new JLabel(w.getStatus());
			label_2.setHorizontalAlignment(SwingConstants.LEFT);
			label_2.setFont(new Font("Tahoma", Font.PLAIN, 14));
			label_2.setBounds(10, 64, 167, 31);
			panel_1.add(label_2);
			
			JLabel label_3 = new JLabel("Nhiệt độ cao nhất: "+w.getTempMax());
			label_3.setHorizontalAlignment(SwingConstants.LEFT);
			label_3.setFont(new Font("Tahoma", Font.PLAIN, 14));
			label_3.setBounds(10, 125, 167, 31);
			panel_1.add(label_3);
			
			JLabel label_4 = new JLabel("Nhiệt độ thấp nhất: "+w.getTempMin());
			label_4.setHorizontalAlignment(SwingConstants.LEFT);
			label_4.setFont(new Font("Tahoma", Font.PLAIN, 14));
			label_4.setBounds(10, 186, 167, 31);
			panel_1.add(label_4);
			
			x+=197;
		}
	}
	
	private void resetWhetherInfo(Whether we) {
		if(we == null) return;
		panel.removeAll();
		
		JLabel lblLoc = new JLabel("Địa điểm");
		lblLoc.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblLoc.setBounds(10, 11, 135, 31);
		panel.add(lblLoc);
		
		JLabel lblDts = new JLabel("Thời gian");
		lblDts.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblDts.setBounds(10, 75, 135, 31);
		panel.add(lblDts);
		
		JLabel lblDc = new JLabel("Thời tiết");
		lblDc.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblDc.setBounds(10, 139, 135, 31);
		panel.add(lblDc);
		
		JLabel lblVLoc = new JLabel(we.getWhetherInfo().getLoc());
		lblVLoc.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblVLoc.setBounds(187, 11, 427, 31);
		panel.add(lblVLoc);
		
		JLabel ilblVDts = new JLabel(we.getWhetherInfo().getDts());
		ilblVDts.setFont(new Font("Tahoma", Font.PLAIN, 14));
		ilblVDts.setBounds(187, 75, 427, 31);
		panel.add(ilblVDts);
		
		JLabel lblVDc = new JLabel(we.getWhetherInfo().getDc());
		lblVDc.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblVDc.setBounds(187, 139, 427, 31);
		panel.add(lblVDc);
		
		JLabel lblTm = new JLabel("Nhiệt độ");
		lblTm.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblTm.setBounds(642, 11, 135, 31);
		panel.add(lblTm);
		
		JLabel lblHm = new JLabel("Khả năng mưa");
		lblHm.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblHm.setBounds(642, 59, 135, 31);
		panel.add(lblHm);
		
		JLabel lblPp = new JLabel("Độ ẩm");
		lblPp.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblPp.setBounds(642, 107, 135, 31);
		panel.add(lblPp);
		
		JLabel lblWs = new JLabel("Gió");
		lblWs.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblWs.setBounds(642, 159, 135, 31);
		panel.add(lblWs);
		
		JLabel lblVTm = new JLabel(we.getWhetherInfo().getTm());
		lblVTm.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblVTm.setBounds(787, 11, 152, 31);
		panel.add(lblVTm);
		
		JLabel lblVHm = new JLabel(we.getWhetherInfo().getHm());
		lblVHm.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblVHm.setBounds(787, 59, 152, 31);
		panel.add(lblVHm);
		
		JLabel lblVPp = new JLabel(we.getWhetherInfo().getPp());
		lblVPp.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblVPp.setBounds(787, 107, 152, 31);
		panel.add(lblVPp);
		
		JLabel lblVWs = new JLabel(we.getWhetherInfo().getWs());
		lblVWs.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblVWs.setBounds(787, 159, 152, 31);
		panel.add(lblVWs);
		
		panel.revalidate();
		panel.repaint();
		if(InfoSystem.autoChangeBG == true) {
			Tool.setBGWithWhether();
		}
	}
}
