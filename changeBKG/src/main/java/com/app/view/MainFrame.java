package com.app.view;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import com.app.InfoSystem;
import com.app.common.Tool;
import com.app.config.Config;
import com.app.controller.alarm.CAlarm;
import com.app.controller.manager.Navigation;
import com.app.model.Alarm;

import javax.swing.JLabel;
import java.awt.Color;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class MainFrame extends JFrame {

	private JPanel contentPane;
	private JPanel viewPanel;
	private JLabel lblDXT;
	private JLabel lblAlarm;
	private JButton nextMusic;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Tool.loadInfoSystem();
					MainFrame frame = new MainFrame();
					InfoSystem.mainFrame = frame;
					InfoSystem.viewPanel = frame.viewPanel;
					InfoSystem.lblAlarm = frame.lblAlarm;
					InfoSystem.lblTopic = frame.lblDXT;
					for(Alarm a : new CAlarm().getAlarms()) {
						Tool.turnOnAlarm(a);
					}
					long timeDelay = 0;
					Calendar ca = Calendar.getInstance();
					ca.setTimeInMillis(ca.getTimeInMillis() + 24*60*60*1000);
					ca.set(Calendar.HOUR, 0);
					ca.set(Calendar.MINUTE, 0);
					ca.set(Calendar.SECOND, 0);
					timeDelay = (ca.getTimeInMillis() - new Date().getTime())/1000;
					InfoSystem.scheduler.scheduleAtFixedRate(new Runnable() {
						
						@Override
						public void run() {
							for(Alarm a : new CAlarm().getAlarms()) {
								Tool.turnOnAlarm(a);
							}
						}
					}, timeDelay, 24*60*60, TimeUnit.SECONDS);
					frame.setVisible(true);
					frame.addWindowListener(new WindowAdapter() {
						public void windowClosing(WindowEvent e) {
							Tool.writeInfoSystem();
							Tool.writeRecentBG();
							Tool.writeAlarm(InfoSystem.als);
							if(InfoSystem.isRunThreadUpdate == true) {
								InfoSystem.scheduler.shutdownNow();
							}
						}
					});
					Tool.runConfig();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public MainFrame() {
		this.setTitle(InfoSystem.titleApp);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1250, 768);
		this.setLocationRelativeTo(null);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		lblDXT = new JLabel(InfoSystem.topic);
		lblDXT.setBounds(10, 11, 530, 31);
		contentPane.add(lblDXT);

		viewPanel = new JPanel();
		viewPanel.setBounds(10, 53, Config.widthPageMain, Config.heightPageMain);
		contentPane.add(viewPanel);
		viewPanel.setLayout(null);

		new Navigation().goToMainPage(viewPanel);

		lblAlarm = new JLabel("");
		lblAlarm.setBackground(Color.YELLOW);
		lblAlarm.setBounds(644, 11, 580, 31);
		contentPane.add(lblAlarm);

		nextMusic = new JButton("Bài tiếp");
		nextMusic.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (InfoSystem.isPlayMusic == true) {
					Tool.player.close();
				}
			}
		});
		nextMusic.setBounds(550, 11, 89, 31);
		contentPane.add(nextMusic);

	}
}
