package com.app.config;

public class Config {
	public static final String linkCloneWhether = "https://www.google.com.vn/search?q=thời+tiết";
	public static final String pathOrigin = "";
	public static final String prefixFile = "";

	public static final int widthPageMain = 1214;
	public static final int heightPageMain = 665;

	public static final int widthPageFunc = 1194;
	public static final int heightPageFunc = 598;
	
	public static final int tabWhether = 1;
	public static final int tabChanger = 2;
	public static final int tabAlarm = 3;
	public static final int tabManager = 4;
	public static final int tabSetting = 5;
	
	public final static String topic = "Hãy thông cảm cho một LTV hay nói tục";
	public static final String titleApp = "DXTAPP";
	
	public final static int timeUpdate = 15;
	
	public static final String pathSun = "image\\sun.jpg";
	public static final String pathHaveSun = "image\\conang.jpg";
	public static final String pathSunCloud = "image\\cloudy.jpg";
	public static final String pathMuchCloud = "image\\much_cloudy.jpg";
	public static final String pathCool = "image\\cool.jpg";
	public static final String pathCold = "image\\cold.jpg";
	public static final String pathGiongBao = "image\\giongbao.jpg";
	public static final String pathRain = "image\\rain.jpg";
	public static final String pathOther = "image\\other.jpg";
	
	public static int huyen = 0;
	public static int xa = 0;
}
