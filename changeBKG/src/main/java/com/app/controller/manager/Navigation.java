package com.app.controller.manager;

import javax.swing.JPanel;

import com.app.InfoSystem;
import com.app.config.Config;
import com.app.impl.INavigation;
import com.app.view.VAlarm;
import com.app.view.VChangeBG;
import com.app.view.VMain;
import com.app.view.VManager;
import com.app.view.VSetting;
import com.app.view.VWhether;

public class Navigation implements INavigation{

	public void goToMainPage(JPanel parent) {
		parent.removeAll();
		VMain main = new VMain(parent);
		parent.add(main);
		parent.revalidate();
		parent.repaint();
		InfoSystem.currentTab = Config.tabWhether;
	}

	public void goToSettingPage(JPanel parent) {
		parent.removeAll();
		VSetting vsetting = new VSetting(parent);
		parent.add(vsetting);
		parent.revalidate();
		parent.repaint();
		InfoSystem.currentTab = Config.tabSetting;
	}

	public void goToManagerPage(JPanel parent) {
		parent.removeAll();
		VManager vmanager = new VManager(parent);
		parent.add(vmanager);
		parent.revalidate();
		parent.repaint();
		InfoSystem.currentTab = Config.tabManager;
	}

	public void goToWhetherPage(JPanel parent) {
		VWhether main = new VWhether(parent);
		parent.removeAll();
		parent.add(main);
		parent.revalidate();
		parent.repaint();
		InfoSystem.currentTab = Config.tabWhether;
	}

	public void goToAlarmPage(JPanel parent) {
		parent.removeAll();
		VAlarm valarm = new VAlarm(parent);
		parent.add(valarm);
		parent.revalidate();
		parent.repaint();
		InfoSystem.currentTab = Config.tabAlarm;
	}

	public void goToChageBGDesktop(JPanel parent) {
		parent.removeAll();
		VChangeBG vchangeBG = new VChangeBG(parent);
		parent.add(vchangeBG);
		parent.revalidate();
		parent.repaint();
		InfoSystem.currentTab = Config.tabChanger;
	}
	
}
