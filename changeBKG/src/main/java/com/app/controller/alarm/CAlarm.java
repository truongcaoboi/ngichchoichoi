package com.app.controller.alarm;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

import com.app.impl.IAlarm;
import com.app.model.Alarm;
import com.google.gson.Gson;

public class CAlarm implements IAlarm{
	@Override
	public List<Alarm> getAlarms(){
		List<Alarm> result = new ArrayList<Alarm>();
		BufferedReader reader = null;
		try {
			reader = new BufferedReader(new FileReader("data.dxt"));
			String str = reader.readLine();
			int count = 0;
			while(str != null) {
				if(str.equals("")) {
					str = reader.readLine();
					continue;
				}
				Alarm  a = new Gson().fromJson(str, Alarm.class);
				a.setId(count+1);
				result.add(a);
				count++;
				str = reader.readLine();
			}
			reader.close();
		}catch(Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	@Override
	public Alarm getAlarmsById(int id, List<Alarm> as) {
		for(Alarm a:as) {
			if(a.getId() == id) {
				return a;
			}
		}
		return null;
	}

	@Override
	public void saveAlarm(Alarm a, JTable table) {
		int index;
		int count = table.getRowCount();
		for(int i =0;i<count;i++) {
			if(a.getId() == (Integer)table.getModel().getValueAt(i, 0)) {
				Object [] info = this.getInfoAlarm(a);
				for(int j =0;j<info.length;j++) {
					table.getModel().setValueAt(info[j], i, j);
				}
			}
		}
	}

	@Override
	public void addAlarm(Alarm a, JTable table) {
		DefaultTableModel model = (DefaultTableModel) table.getModel();
		model.addRow(this.getInfoAlarm(a));
		table.revalidate();
		table.repaint();
		System.out.println("ok");
	}

	@Override
	public Object[][] getInfoAlarms(List<Alarm> as) {
		List<Object[]> result = new ArrayList<Object[]>();
		int count = 0;
		for(Alarm a: as) {
			a.setId(count+1);
			count++;
			result.add(this.getInfoAlarm(a));
		}
		return result.toArray(new Object[result.size()][]);
	}

	@Override
	public Object[] getInfoAlarm(Alarm a) {
		List<Object> result = new ArrayList<Object>();
		result.add(a.getId());
		Calendar ca = Calendar.getInstance();
		ca.setTimeInMillis(a.getTime());
		String time = "";
		time+=ca.get(Calendar.HOUR_OF_DAY)+":"+ca.get(Calendar.MINUTE)+" "+ca.get(Calendar.DAY_OF_MONTH)+"/"+ca.get(Calendar.MONTH)+"/"+ca.get(Calendar.YEAR);
		result.add(time);
		String [] mode = new String[] {"Không lặp lại","Hàng ngày","Hàng tuần","Hàng tháng","Hàng năm","Tùy chỉnh"};
		if(a.getType() == mode.length -1) {
			String mode2 [] = new String[] {"ngày","tuần","tháng","năm"};
			String cc = "Lặp sau "+a.getTimeLoop()+" "+mode2[a.getTypeLoop()];
			if(a.getTypeLoop() == 1) {
				if(a.getDateLoop().length > 0) {
					cc+=" vào các ngày";
					for(int i : a.getDateLoop()) {
						if(i== 8) {
							cc+=" Chủ nhật";
						}else {
							cc+=" Thứ "+i;
						}
					}
				}
			}else if(a.getTypeLoop() == 2) {
				Calendar cac = Calendar.getInstance();
				cac.setTimeInMillis(a.getTime());
				String a1 = " vào ngày " + cac.get(Calendar.DAY_OF_MONTH) + " trong tháng";
				String b = " vào " + getThu(cac.get(Calendar.DAY_OF_WEEK)) + " của tuần thứ " + cac.get(Calendar.WEEK_OF_MONTH)
						+ " trong tháng";
				String c = " vào " + getThu(cac.get(Calendar.DAY_OF_WEEK)) + " thứ " + cac.get(Calendar.DAY_OF_WEEK_IN_MONTH)
						+ " trong tháng";
				String mode3[] = new String[] { a1, b, c };
				cc+= mode3[a.getTypeLoopByMonth()];
			}
			result.add(cc);
		}else {
			result.add(mode[a.getType()]);
		}
		if(a.getType() == 5) {
			if(a.getTypeEnd() == 1) {
				result.add("Không bao giờ");
			}else if(a.getTypeEnd() == 2) {
				Calendar cc = Calendar.getInstance();
				cc.setTimeInMillis(a.getTimeFinish());
				String dm = "Kết thúc vào ngày "+cc.get(Calendar.DAY_OF_MONTH)+"/"+cc.get(Calendar.MONTH)+"/"+cc.get(Calendar.YEAR);
				result.add(dm);
			}else {
				String dm = "Kết thúc sau"+a.getTimeLimitExe()+" lần lặp";
				result.add(dm);
			}
		}else {
			result.add("Không set <Không bao giờ>");
		}
		if(a.getStatus() == 0) {
			result.add("Đã bật");
		}else {
			result.add("Đã tắt");
		}
		return result.toArray();
	}

	@Override
	public void deleteAlarm(Alarm a, JTable table) {
		
	}

	@Override
	public void turnOnAlarm(Alarm a, JTable table) {
		
	}

	@Override
	public void turnOffAlarm(Alarm a, JTable table) {
		
	}
	
	private String getThu(int x) {
		switch (x) {
		case 1:
			return "Chủ nhật";
		case 2:
			return "Thứ hai";
		case 3:
			return "Thứ ba";
		case 4:
			return "Thứ tư";
		case 5:
			return "Thứ năm";
		case 6:
			return "Thứ sáu";
		case 7:
			return "Thứ bảy";
		default:
			return "Thứ lozz";
		}
	}
}
