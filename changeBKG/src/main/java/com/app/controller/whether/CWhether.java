package com.app.controller.whether;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.app.InfoSystem;
import com.app.config.Config;
import com.app.model.Huyen;
import com.app.model.Whether;
import com.app.model.WhetherInfo;
import com.app.model.WhetherOther;
import com.app.model.Xa;
import com.google.gson.Gson;

public class CWhether {
	public Whether getWhether() {
		try {
			File file = new File("whetherToday");
			BufferedReader reader = new BufferedReader(new FileReader(file));
			Calendar now = Calendar.getInstance();
			Calendar createTime = Calendar.getInstance();
			createTime.setTime(new Date(Long.parseLong(reader.readLine())));
			Whether w = null;
			if (now.get(Calendar.DAY_OF_YEAR) == createTime.get(Calendar.DAY_OF_YEAR)
					& now.get(Calendar.HOUR_OF_DAY) == createTime.get(Calendar.HOUR_OF_DAY)) {
				w = new Gson().fromJson(reader.readLine(), Whether.class);
			}
			reader.close();
			if (w != null) {
				System.out.println("read from file");
				return w;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return getFromWeb();
	}
	
	public Whether getFromWeb() {
		WhetherInfo wei = getWhetherInffo();
		List<WhetherOther> weos = getWhetherOthers();
		if (wei == null | weos == null) {
			return null;
		} else {
			System.out.println("read from web");
			return new Whether(wei, weos);
		}
	}

	private String getLinkClone() {
		String link = Config.linkCloneWhether;
		Huyen hc = null;
		Xa xc = null;
		for (Huyen h : InfoSystem.hs) {
			if (h.getCode() == InfoSystem.huyen) {
				hc = h;
				break;
			}
		}
		for (Xa x : hc.getXas()) {
			if (x.getCode() == InfoSystem.xa) {
				xc = x;
			}
		}
		link += "+" + xc.getSearch() + "+" + hc.getSearch() + "+hà+nội&num=10";
		return link;
		//return "https://www.google.com.vn/search?sxsrf=ALeKk03EMfLKERP2dRGTSkavpRqFA3fSjQ%3A1593153716345&source=hp&ei=tJj1XvznEvCVr7wP_76XqAg&q=th%E1%BB%9Di+ti%E1%BA%BFt&oq=th%E1%BB%9Di+ti%E1%BA%BFt&gs_lcp=CgZwc3ktYWIQAzIMCCMQJxCdAhBGEIACMgQIIxAnMgQIIxAnMgUIABCDATIFCAAQsQMyBQgAELEDMgIIADIFCAAQgwEyBQgAELEDMgIIADoHCCMQJxCdAjoECAAQQzoECAAQCjoHCCMQ6gIQJzoHCAAQRhCAAlD9AljGKGCIKmgNcAB4BIABjgOIAaMckgEHNS44LjIuNJgBAKABAaoBB2d3cy13aXqwAQo&sclient=psy-ab&ved=0ahUKEwj8kdWK8J7qAhXwyosBHX_fBYUQ4dUDCAc&uact=5";
	}

	private WhetherInfo getWhetherInffo() {
		String user = "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/86.0.172 Chrome/80.0.3987.172 Safari/537.36";
		String link = getLinkClone();
		try {
			Connection con = Jsoup.connect(link).userAgent(user).timeout(3000);
			Document doc = con.get();
			//System.out.println(doc.html());
			String loc = doc.getElementById("wob_loc").text();
			String dts = doc.getElementById("wob_dts").text();
			String dc = doc.getElementById("wob_dc").text();
			String tm = doc.getElementById("wob_tm").text();
			String pp = doc.getElementById("wob_pp").text();
			String hm = doc.getElementById("wob_hm").text();
			String ws = doc.getElementById("wob_ws").text();
			WhetherInfo we = new WhetherInfo(loc, dts, dc, tm, pp, hm, ws);
			return we;
		} catch (Exception e) {
			System.out.println(link);
			e.printStackTrace();
			return null;
		}
	}

	private List<WhetherOther> getWhetherOthers() {
		String link = getLinkClone();
		String user = "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/86.0.172 Chrome/80.0.3987.172 Safari/537.36";
		List<WhetherOther> wes = new ArrayList<WhetherOther>();
		try {
			Document doc = Jsoup.connect(link).userAgent(user).timeout(3000).get();
			Elements els = doc.getElementById("wob_dp").children();
			for (Element el : els) {
				String time = el.children().get(0).text();
				String status = el.children().get(1).child(0).attr("alt");
				String tempMax = el.children().get(2).getElementsByClass("vk_gy").get(0).getElementsByClass("wob_t")
						.get(0).text();
				String tempMin = el.children().get(2).getElementsByClass("QrNVmd").get(0).getElementsByClass("wob_t")
						.get(0).text();
				wes.add(new WhetherOther(time, status, tempMax, tempMin));
			}
			return wes;
		} catch (Exception e) {
			System.out.println(link);
			e.printStackTrace();
			return null;
		}
	}
}
