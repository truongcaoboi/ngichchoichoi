package com.app;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.ThreadFactory;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import com.app.model.Alarm;
import com.app.model.Huyen;
import com.app.model.Whether;

public class InfoSystem {
	//prop
	public static int currentTab = 0;
	public static boolean autoUpdateWhether = false;
	public static boolean autoChangeBG = false;
	public static String chooseFolderBG = "";
	public static String linkClone = null;
	public static String topic = "";
	public static String titleApp = "";
	public static boolean playMusic = false;
	public static String chooseFolderMS = "";
	public static boolean isRunThreadUpdate = false;
	public static boolean isPlayMusic = false;
	public static boolean isShutUp = false;
	public static int xa = 0;
	public static int huyen = 0;
	
	public static String pathSun = "image\\sun.jpg";
	public static String pathHaveSun = "image\\conang.jpg";
	public static String pathSunCloud = "image\\cloudy.jpg";
	public static String pathMuchCloud = "image\\much_cloudy.jpg";
	public static String pathCool = "image\\cool.jpg";
	public static String pathCold = "image\\cold.jpg";
	public static String pathGiongBao = "image\\giongbao.jpg";
	public static String pathRain = "image\\rain.jpg";
	public static String pathOther = "image\\other.jpg";
	
	//object
	public static Whether we = null;
	public static List<String> listRecentBG = new ArrayList<String>();
	public static List<Alarm> als = new ArrayList<Alarm>();
	public static JFrame mainFrame = null;
	public static JPanel viewPanel = null;
	public static JLabel lblTopic = null;
	public static JLabel lblAlarm = null;
	public static List<Huyen> hs = new ArrayList<Huyen>();
	
	//Thread
	public static ScheduledThreadPoolExecutor scheduler = (ScheduledThreadPoolExecutor) Executors.newScheduledThreadPool(2, new ThreadFactory() {
		public Thread newThread(Runnable r) {
			Thread t = new Thread(r);
			t.setDaemon(true);
			return t;
		}
	});
	public static List<ScheduledFuture<?>> tasks = new ArrayList<ScheduledFuture<?>>();
	public static Map<String ,ScheduledFuture<?>> taskIds = new HashMap<String, ScheduledFuture<?>>();
}
