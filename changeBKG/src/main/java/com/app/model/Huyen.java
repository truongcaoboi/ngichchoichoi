package com.app.model;

import java.util.List;

public class Huyen {
	private int code;
	private String name;
	private String search;
	private List<Xa> xas;
	public int getCode() {
		return code;
	}
	public void setCode(int code) {
		this.code = code;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getSearch() {
		return search;
	}
	public void setSearch(String search) {
		this.search = search;
	}
	public List<Xa> getXas() {
		return xas;
	}
	public void setXas(List<Xa> xas) {
		this.xas = xas;
	}
	
}
