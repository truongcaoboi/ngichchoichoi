package com.app.model;

public class WhetherInfo {
	public WhetherInfo(String loc, String dts, String dc, String tm, String hm, String pp, String ws) {
		this.loc = loc;
		this.dts = dts;
		this.dc = dc;
		this.tm = tm;
		this.hm = hm;
		this.pp = pp;
		this.ws = ws;
	}
	
	private String loc;
	private String dts;
	private String dc;
	private String tm;
	private String hm;
	private String pp;
	private String ws;
	public String getLoc() {
		return loc;
	}
	public void setLoc(String loc) {
		this.loc = loc;
	}
	public String getDts() {
		return dts;
	}
	public void setDts(String dts) {
		this.dts = dts;
	}
	public String getDc() {
		return dc;
	}
	public void setDc(String dc) {
		this.dc = dc;
	}
	public String getTm() {
		return tm;
	}
	public void setTm(String tm) {
		this.tm = tm;
	}
	public String getHm() {
		return hm;
	}
	public void setHm(String hm) {
		this.hm = hm;
	}
	public String getPp() {
		return pp;
	}
	public void setPp(String pp) {
		this.pp = pp;
	}
	public String getWs() {
		return ws;
	}
	public void setWs(String ws) {
		this.ws = ws;
	}
}