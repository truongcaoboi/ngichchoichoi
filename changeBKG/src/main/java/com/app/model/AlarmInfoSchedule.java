package com.app.model;

import java.util.concurrent.ScheduledFuture;

public class AlarmInfoSchedule {
	private Alarm alarm;
	private ScheduledFuture<?> schedule;
	public Alarm getAlarm() {
		return alarm;
	}
	public void setAlarm(Alarm alarm) {
		this.alarm = alarm;
	}
	public ScheduledFuture<?> getSchedule() {
		return schedule;
	}
	public void setSchedule(ScheduledFuture<?> schedule) {
		this.schedule = schedule;
	}
	
	public AlarmInfoSchedule(Alarm a, ScheduledFuture<?> s) {
		alarm = a;
		schedule = s;
	}
}
