package com.app.model;

import java.util.List;

public class Whether {
	public Whether(WhetherInfo info, List<WhetherOther> other) {
		this.whetherInfo = info;
		this.whetherOthers = other;
	}
	
	private WhetherInfo whetherInfo;
	private List<WhetherOther> whetherOthers;
	
	public WhetherInfo getWhetherInfo() {
		return whetherInfo;
	}
	public void setWhetherInfo(WhetherInfo whetherInfo) {
		this.whetherInfo = whetherInfo;
	}
	public List<WhetherOther> getWhetherOthers() {
		return whetherOthers;
	}
	public void setWhetherOthers(List<WhetherOther> whetherOthers) {
		this.whetherOthers = whetherOthers;
	}
}
