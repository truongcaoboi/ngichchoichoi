package com.app.model;

public class WhetherOther {
	public WhetherOther(String time, String status, String tempMax, String tempMin) {
		this.time = time;
		this.status = status;
		this.tempMax = tempMax;
		this.tempMin = tempMin;
	}
	
	private String time;
	private String status;
	private String tempMax;
	private String tempMin;
	public String getTime() {
		return time;
	}
	public void setTime(String time) {
		this.time = time;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getTempMax() {
		return tempMax;
	}
	public void setTempMax(String tempMax) {
		this.tempMax = tempMax;
	}
	public String getTempMin() {
		return tempMin;
	}
	public void setTempMin(String tempMin) {
		this.tempMin = tempMin;
	}
}
