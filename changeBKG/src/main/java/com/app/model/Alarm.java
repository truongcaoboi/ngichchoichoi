package com.app.model;

import java.util.UUID;

public class Alarm {
	private int id;//id
	private String mes;//dong thong bao
	private long time;//thoi gian goi
	private int type;//loai
	private int typeLoop;//loai lap
	private int timeLoop;//so lan lap
	private int [] dateLoop;//nhung ngay lap
	private int typeLoopByMonth;
	private int timeExe;//so lan thuc hien
	private int timeLimitExe;//so lan thuc hien toi da
	private long timeFinish;//thoi gian ket thuc
	private String end;
	private int typeEnd;//kieeur ket thuc
	private UUID uid = UUID.randomUUID();
	public UUID getUid() {
		return uid;
	}
	public void setUid(UUID uid) {
		this.uid = uid;
	}
	public int getTypeEnd() {
		return typeEnd;
	}
	public void setTypeEnd(int typeEnd) {
		this.typeEnd = typeEnd;
	}
	public String getEnd() {
		return end;
	}
	public void setEnd(String end) {
		this.end = end;
	}
	private int status;//trang thai
	public int getId() {
		return id;
	}
	public int getTypeLoopByMonth() {
		return typeLoopByMonth;
	}
	public void setTypeLoopByMonth(int typeLoopByMonth) {
		this.typeLoopByMonth = typeLoopByMonth;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getMes() {
		return mes;
	}
	public void setMes(String mes) {
		this.mes = mes;
	}
	public long getTime() {
		return time;
	}
	public void setTime(long time) {
		this.time = time;
	}
	public int getType() {
		return type;
	}
	public void setType(int type) {
		this.type = type;
	}
	public int getTypeLoop() {
		return typeLoop;
	}
	public void setTypeLoop(int typeLoop) {
		this.typeLoop = typeLoop;
	}
	public int getTimeLoop() {
		return timeLoop;
	}
	public void setTimeLoop(int timeLoop) {
		this.timeLoop = timeLoop;
	}
	public int[] getDateLoop() {
		return dateLoop;
	}
	public void setDateLoop(int[] dateLoop) {
		this.dateLoop = dateLoop;
	}
	public int getTimeExe() {
		return timeExe;
	}
	public void setTimeExe(int timeExe) {
		this.timeExe = timeExe;
	}
	public int getTimeLimitExe() {
		return timeLimitExe;
	}
	public void setTimeLimitExe(int timeLimitExe) {
		this.timeLimitExe = timeLimitExe;
	}
	public long getTimeFinish() {
		return timeFinish;
	}
	public void setTimeFinish(long timeFinish) {
		this.timeFinish = timeFinish;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
}
