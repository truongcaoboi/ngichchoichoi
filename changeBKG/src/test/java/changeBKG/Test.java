package changeBKG;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.HashMap;
import java.util.UUID;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import javax.swing.JOptionPane;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.app.InfoSystem;
import com.app.model.Alarm;
import com.app.model.Huyen;
import com.app.model.Xa;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.sun.jna.Native;
import com.sun.jna.win32.*;

import javazoom.jl.decoder.JavaLayerException;
import javazoom.jl.player.Player;

public class Test {
	public static void main(String args[]) {
		String str = "Hà Nội nghìn năm văn hiến";
		String str1 = "Đất nước con người việt nam";
		BufferedReader reader = null;
		try {
			FileOutputStream out = new FileOutputStream(new File("test.test"));
			out.write(str.getBytes("UTF-8"));
			out.write("\r\n".getBytes());
			out.write(str1.getBytes());
			out.flush();
			out.close();
			//reader = new BufferedReader(new FileReader("test.test"));
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	public static JsonObject testCloneData() {
		String link = "https://www.google.com/search?q=thời tiết";
		try {
			Document doc = Jsoup.connect(link).get();
			String location = doc.getElementById("wob_loc").text();
			String dst = doc.getElementById("wob_dts").text();
			String status = doc.getElementById("wob_dc").text();
			String temp = doc.getElementById("wob_tm").text();
			String rainable = doc.getElementById("wob_pp").text();
			String wet = doc.getElementById("wob_hm").text();
			String windy = doc.getElementById("wob_ws").text();

			Elements els = doc.getElementById("wob_dp").children();

			System.out.println("Location: " + location);
			System.out.println("Time: " + dst);
			System.out.println("Status: " + status);
			System.out.println("Temp: " + temp);
			System.out.println("rain: " + rainable);
			System.out.println("wet: " + wet);
			System.out.println("windy: " + windy);

			for (Element e : els) {
				System.out.println("Time: " + e.children().get(0).text());
				System.out.println("Status: " + e.children().get(1).child(0).attr("alt"));
				System.out.println("Temp1: " + e.children().get(2).getElementsByClass("vk_gy").get(0)
						.getElementsByClass("wob_t").get(0).text());
				System.out.println("Temp2: " + e.children().get(2).getElementsByClass("QrNVmd").get(0)
						.getElementsByClass("wob_t").get(0).text());
				System.out.println("===========================================");
			}

			JsonObject jo = new JsonObject();
			jo.addProperty("loc", location);
			jo.addProperty("dst", dst);
			jo.addProperty("dc", status);
			jo.addProperty("tm", temp);
			jo.addProperty("pp", rainable);
			jo.addProperty("hm", wet);
			jo.addProperty("ws", windy);
			return jo;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public static void testChangeBackground() {
		String path = "E:\\laptrinhjava\\luyentap java\\changeBKG\\U1-D-1-1-sfaxrjpmospyzrcn.jpg";
		SPI.INSTANCE.SystemParametersInfo(0x0014, 0, path, 1);
	}

	public interface SPI extends StdCallLibrary {

		// from MSDN article
		long SPI_SETDESKWALLPAPER = 20;
		long SPIF_UPDATEINIFILE = 0x01;
		long SPIF_SENDWININICHANGE = 0x02;
		@SuppressWarnings("serial")
		SPI INSTANCE = (SPI) Native.loadLibrary("user32", SPI.class, new HashMap<String, Object>() {
			{
				put(OPTION_TYPE_MAPPER, W32APITypeMapper.UNICODE);
				put(OPTION_FUNCTION_MAPPER, W32APIFunctionMapper.UNICODE);
			}
		});

		boolean SystemParametersInfo(int a, int b, String pvParam, int c);
	}
	
	public static void testLoadHuyen() {
		String jsonStr = "";
		BufferedReader reader = null;
		try {
			reader = new BufferedReader(new FileReader("huyenxa.json"));
			jsonStr = reader.readLine();
			reader.close();
			Huyen [] hs = new Gson().fromJson(jsonStr, Huyen[].class);
			for(Huyen h : hs) {
				System.out.println("ch: "+h.getCode());
				System.out.println("nh "+h.getName());
				System.out.println("list xa");
				for(Xa x : h.getXas()) {
					System.out.println("cx: "+x.getCode());
					System.out.println("nx: "+x.getName());
				}
				System.out.println("====================================");
			}
		}catch(Exception e) {
			e.printStackTrace();
		}
	}
}
